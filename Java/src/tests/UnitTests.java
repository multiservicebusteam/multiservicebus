package tests;

import org.junit.Test;
import net.codingbytodesign.multiservicebus.common.Action;
import net.codingbytodesign.multiservicebus.servicelocator.ServiceLocator;
import net.codingbytodesign.multiservicebus.common.EventListener;
import sun.jvm.hotspot.utilities.Assert;

import java.util.*;

public class UnitTests {
    public class TestClass {
        private int id;

        public TestClass() {
            id = -1;
        }

        public int getId() {
            return id;
        }

        public void setId(int val) {
            id = val;
        }
    }

    @Test
    public void test001ServiceLocator() {
        final int testId = 7;
        final boolean[] notified = {false};
        String testKey = "testClass";
        EventListener changed = new EventListener() {
            @Override
            public void doNotify() {
                notified[0] = true;
            }
        };
        ServiceLocator locator = ServiceLocator.getInstance(changed);
        TestClass testClass = new TestClass();
        testClass.setId(testId);
        locator.register(testClass, testKey);
        TestClass testClassFromLocator = locator.getService(testKey);
        Assert.that(testClassFromLocator.getId() == testId, "Id incorrect, service locator does not work!");
        Assert.that(notified[0], "Observer pattern does not work!");
    }

    @Test
    public void test002ServiceLocator() {
        final int testId0 = 7;
        final int testId1 = 11;
        final boolean[] notified = {false};
        String testKey = "testClass";
        EventListener changed = new EventListener() {
            @Override
            public void doNotify() {
                notified[0] = true;
            }
        };
        ServiceLocator locator = ServiceLocator.getInstance(changed);
        TestClass testClass0 = new TestClass();
        testClass0.setId(testId0);
        locator.registerMulti(testClass0, testKey);
        TestClass testClass1 = new TestClass();
        testClass1.setId(testId1);
        locator.registerMulti(testClass1, testKey);
        List<TestClass> testClassesFromLocator = locator.getServices(testKey);
        final int id0 = testClassesFromLocator.get(0).getId();
        final int id1 = testClassesFromLocator.get(1).getId();
        Assert.that(id0 == testId0 || id0 == testId1, "Id incorrect, service locator does not work!");
        Assert.that(id1 == testId0 || id1 == testId1, "Id incorrect, service locator does not work!");
        Assert.that(notified[0], "Observer pattern does not work!");
    }

    public class TestAction<E> extends Action<E>{
        public boolean notified = false;
        @Override
        public <E1> void invoke(E1 arg) {
            notified = true;
        }
    }

    @Test
    public void test003ServiceLocator() {
        final int testId = 7;
        final boolean[] notified = {false};
        String testKey = "testClass";
        EventListener changed = new EventListener() {
            @Override
            public void doNotify() {
                notified[0] = true;
            }
        };
        ServiceLocator locator = ServiceLocator.getInstance(changed);
        TestClass testClass = new TestClass();
        testClass.setId(testId);
        TestAction<TestClass[]> testAction = new TestAction<TestClass[]>();
        locator.hangStateChangedMessage(testKey, testAction);
        locator.register(testClass, testKey);
        TestClass testClassFromLocator = locator.getService(testKey);
        Assert.that(testClassFromLocator.getId() == testId, "Id incorrect, service locator does not work!");
        Assert.that(notified[0], "Observer pattern does not work!");
        Assert.that(testAction.notified, "Action does not work!");
    }
}
