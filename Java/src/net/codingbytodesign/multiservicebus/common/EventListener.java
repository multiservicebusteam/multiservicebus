package net.codingbytodesign.multiservicebus.common;

public class EventListener {
    public Object state0 = null;
    public Object state1 = null;

    public EventListener () {}

    public EventListener (Object state0, Object state1) {
        this.state0 = state0;
        this.state1 = state1;
    }

    public void doNotify() {}
}
