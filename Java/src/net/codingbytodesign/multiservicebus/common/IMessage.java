package net.codingbytodesign.multiservicebus.common;

/**
 * Created by piotrsowa on 22/04/15.
 */
public interface IMessage {
    byte[] Serialize();
    void Deserialize(byte[] data);
}
