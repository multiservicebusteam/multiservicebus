package net.codingbytodesign.multiservicebus.common;

/**
 * Created by piotrsowa on 22/04/15.
 */
public interface IDisposable {
    public void Dispose();
}
