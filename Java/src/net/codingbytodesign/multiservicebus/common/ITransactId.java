package net.codingbytodesign.multiservicebus.common;

public interface ITransactId {
    public long getValue();
}
