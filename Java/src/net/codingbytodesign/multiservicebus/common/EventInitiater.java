package net.codingbytodesign.multiservicebus.common;

import net.codingbytodesign.multiservicebus.common.EventListener;

import java.util.ArrayList;
import java.util.List;

public class EventInitiater {
    List<EventListener> events = null;

    public EventInitiater() {
        events = new ArrayList<EventListener>();
    }

    public void addListener(EventListener toAdd){
        events.add(toAdd);
    }

    public void doNotify() {
        if (events.isEmpty()){
            return;
        }
        for(EventListener event : events) {
            event.doNotify();
        }
    }
}
