package net.codingbytodesign.multiservicebus.common;

public abstract class Action<E> {
    public <E> void invoke(E arg) { }
}

