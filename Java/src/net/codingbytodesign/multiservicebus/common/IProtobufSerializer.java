package net.codingbytodesign.multiservicebus.common;

// todo: implement this... serialization 2-way with protobuf to and from byte[], but how? :).
public interface IProtobufSerializer {
    public <T> byte[] serialize(T object);

    public <T> T deserialize(byte[] data);
}
