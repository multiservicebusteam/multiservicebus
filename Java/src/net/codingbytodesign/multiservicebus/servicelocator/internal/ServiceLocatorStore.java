package net.codingbytodesign.multiservicebus.servicelocator.internal;

import java.util.*;
import net.codingbytodesign.multiservicebus.common.*;

public class ServiceLocatorStore {
    public List<Object> store = null;
    public EventInitiater eventsInitiater = null;
    String storeKey = null;
    Object storeService = null;

    public <T> ServiceLocatorStore(String key, T service, net.codingbytodesign.multiservicebus.common.EventListener eventsListener) {
        storeKey = key;
        store = new ArrayList<Object>();
        if (service != null) {
            store.add(service);
            storeService = service;
        }
        eventsInitiater = new EventInitiater();
        eventsInitiater.addListener(eventsListener);
    }

    public <T> boolean addService(String serviceKey, T service) {
        if (storeKey == serviceKey) {
            if (service != null) {
                store.add(service);

                if (service == null)
                    storeService = service;

                eventsInitiater.doNotify();

                return true;
            }
        }

        return false;
    }

    public <T> boolean replace(String serviceKey, T service) {
        if (storeKey == serviceKey) {
            if (service != null) {
                if (store.isEmpty()) {
                    store.add(service);
                } else {
                    store.add(0, service);
                }

                storeService = service;

                return true;
            }
        }
        return false;
    }
}