package net.codingbytodesign.multiservicebus.servicelocator;

import net.codingbytodesign.multiservicebus.servicelocator.internal.*;
import net.codingbytodesign.multiservicebus.common.EventListener;
import net.codingbytodesign.multiservicebus.common.*;

import java.util.*;
import java.util.concurrent.*;

public class ServiceLocator
{
    static ServiceLocator instance = new ServiceLocator();

    public static ServiceLocator getInstance(EventListener eventsListener) {
        instance.eventsListener = eventsListener;
        instance.eventsInitiater.addListener(eventsListener);
        return instance;
    }

    public ConcurrentHashMap<String, ServiceLocatorStore> services;

    private EventInitiater eventsInitiater = null;
    private EventListener eventsListener = null;

    private ServiceLocator()
    {
        eventsInitiater = new EventInitiater();
        services = new ConcurrentHashMap<String, ServiceLocatorStore>();
    }

    private <T> String getKey(T service, String key) {
        return key != null ? key : service.getClass().getName();
    }

    public <T> boolean register(T service, String key)
    {
        String serviceKey = getKey(service, key);
        ServiceLocatorStore store;
        if ((store = services.get(serviceKey)) != null)
        {
            store.replace(serviceKey, service);
        }
        else
        {
            store = new ServiceLocatorStore(serviceKey, service, eventsListener);
            services.put(serviceKey, store);
        }
        eventsInitiater.doNotify();
        return services.containsKey(serviceKey);
    }

    public <T> boolean registerMulti(T service, String key)
    {
        String serviceKey = getKey(service, key);
        ServiceLocatorStore store;
        if ((store = services.get(serviceKey)) != null)
        {
            store.addService(serviceKey, service);
        }
        else
        {
            store = new ServiceLocatorStore(serviceKey, service, eventsListener);
            services.put(serviceKey, store);
        }
        eventsInitiater.doNotify();
        return services.containsKey(serviceKey);
    }

    public <T> boolean hangStateChangedMessage(String key, Action<T[]> action)
    {
        T service = null;
        String serviceKey = getKey(service, key);
        ServiceLocatorStore store;
        if ((store = services.get(serviceKey)) != null) {
            EventListener listener = new EventListener(store, action){
                @Override
                public void doNotify() {
                    ServiceLocatorStore store = (ServiceLocatorStore)state0;
                    Action<T[]> action = (Action<T[]>)state1;
                    List<T> servicesList = new ArrayList<T>();
                    for(Object serviceObject : store.store) {
                        servicesList.add((T) serviceObject);
                    }
                    action.invoke(servicesList);
                }
            };
            eventsInitiater.addListener(listener);
            return true;
        }
        return false;
    }

    public <T> T getService(String key)
    {
        T service = null;
        String serviceKey = getKey(service, key);
        ServiceLocatorStore store;
        if ((store = services.get(serviceKey)) != null)
        {
            service = (T)store.store.get(0);
            return service;
        }
        return null;
    }

    public <T> List<T> getServices(String key)
    {
        T service = null;
        String serviceKey = getKey(service, key);
        ServiceLocatorStore store;
        if ((store = services.get(serviceKey)) != null)
        {
            List<T> servicesList = new ArrayList<T>();
            for(Object serviceObject : store.store) {
                servicesList.add((T)serviceObject);
            }
            return servicesList;
        }
        return new ArrayList<T>();
    }
}
