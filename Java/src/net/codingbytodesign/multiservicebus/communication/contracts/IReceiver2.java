package net.codingbytodesign.multiservicebus.communication.contracts;

import net.codingbytodesign.multiservicebus.common.*;

public interface IReceiver2<TMessageReq extends IMessage, TMessageRes extends IMessage> extends IDisposable {
    public Action2<ITransactId, TMessageReq> getRecieveAction();

    public void setRecieveAction(Action2<ITransactId, TMessageReq> recieveAction);

    public void responseAsync(ITransactId id, TMessageRes message);
}
