package net.codingbytodesign.multiservicebus.communication.contracts;

import net.codingbytodesign.multiservicebus.common.*;

public interface IRequester2<TMessageReq extends IMessage, TMessageRes extends IMessage> extends IDisposable {
    public Action<TMessageRes> getResponseAction();

    public void setResponseAction(Action<TMessageRes> responseAction);

    public void requestAsync(TMessageReq message, Action<TMessageRes> responseAction);

    public void requestSync(TMessageReq message, Action<TMessageRes> responseAction);
}
