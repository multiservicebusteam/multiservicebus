package net.codingbytodesign.multiservicebus.communication.contracts;

import net.codingbytodesign.multiservicebus.common.*;

public interface IReceiver1<TMessageReq extends IMessage> extends IDisposable {
    public Action<TMessageReq> getRecieveAction();

    public <TMessageRes> void setRecieveAction(Action<TMessageReq> receiveAction);
}
