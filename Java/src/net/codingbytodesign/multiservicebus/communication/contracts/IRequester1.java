package net.codingbytodesign.multiservicebus.communication.contracts;

import net.codingbytodesign.multiservicebus.common.*;

public interface IRequester1<TMessageReq extends IMessage> extends IDisposable {
    public void requestAsync(TMessageReq message);
}
