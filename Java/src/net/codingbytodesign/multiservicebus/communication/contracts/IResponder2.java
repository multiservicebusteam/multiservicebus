package net.codingbytodesign.multiservicebus.communication.contracts;

import net.codingbytodesign.multiservicebus.common.*;

public interface IResponder2<TMessageReq extends IMessage, TMessageRes extends IMessage> extends IDisposable {
    void setResponseFunc(Func<TMessageReq, TMessageRes> responseFunc);
}
