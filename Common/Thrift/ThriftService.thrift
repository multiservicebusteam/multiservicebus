/**
 * The first thing to know about are types. The available types in Thrift are:
 *
 *  bool        Boolean, one byte
 *  byte        Signed byte
 *  i16         Signed 16-bit integer
 *  i32         Signed 32-bit integer
 *  i64         Signed 64-bit integer
 *  double      64-bit floating point value
 *  string      String
 *  binary      Blob (byte array)
 *  map<t1,t2>  Map from one type to another
 *  list<t1>    Ordered list of one type
 *  set<t1>     Set of unique elements of one type
 */

namespace cpp        CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace csharp     CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace d          CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace delphi     CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace erl        CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace go         CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace hs         CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace java       CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace js         CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace perl       CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace php        CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace py         CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace py.twisted CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
namespace rb         CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift

struct ThriftMessage {
  1: binary SerializedMessage
} 

service ThriftService {
  string BeginSession(1:string sessionKey),
  list<ThriftMessage> Consume(1:string sessionKey),
  bool EndSession(1:string sessionKey),
  oneway void Publish(1:string sessionKey, 2: list<ThriftMessage> notifications)
}
