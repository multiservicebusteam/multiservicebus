namespace CodingByToDesign.MultiServiceBus.Helpers
{
    using System;
    using System.Threading;

    internal static class SyncHelpers
    {
        public static void SafeReadAction(this ReaderWriterLockSlim locker, Action action)
        {
            try
            {
                locker.EnterReadLock();
                action();
            }
            finally
            {
                locker.ExitReadLock();
            }
        }

        public static void SafeWriteAction(this ReaderWriterLockSlim locker, Action action)
        {
            try
            {
                locker.EnterWriteLock();
                action();
            }
            finally
            {
                locker.ExitWriteLock();
            }
        }
    }
}