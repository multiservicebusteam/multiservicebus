namespace CodingByToDesign.MultiServiceBus.Communication
{
    using System;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal;
    using Thrift.Transport;

    public class Receiver1<TMessageReq> : IReceiver1<TMessageReq>, IDisposable
        where TMessageReq : IMessage, new()
    {
        private readonly Engine<TMessageReq, NullMessage> engine;
        private TTransport service;
        private readonly EngineNetSub<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage> consumer;
        private readonly EngineNetSubTran<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage> engineNet;

        public Receiver1(int timeout = 5000, int transactionTimeout = 30000)
        {
            engine = new Engine<TMessageReq, NullMessage>(EngineOperateKind.ReceiverOnly, timeout, transactionTimeout);
        }

        public Receiver1(string netTcpRequesterAddress, int timeout = 5000, int transactionTimeout = 30000, int bufferLength = 1000, string sessionKey = null)
        {
            if (string.IsNullOrWhiteSpace(netTcpRequesterAddress))
            {
                throw new ArgumentException("Argument is Empty", "netTcpRequesterAddress");
            }
            else
            {
                consumer = new EngineNetSub<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage>(timeout, 1000, bufferLength, sessionKey);
                consumer.Open(() => new EngineNetPub<TMessage<TMessageReq, NullMessage>>(() => FactoryNetComm.CreateClient(netTcpRequesterAddress, out service)));

                engineNet = new EngineNetSubTran<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage>(consumer, transactionTimeout);
            }
        }

        public Action<TMessageReq> RecieveAction
        {
            get
            {
                if (engine != null)
                {
                    return engine.RecieveOnlyAction;
                }
                else
                {
                    return engineNet.RecieveOnlyAction;
                }
            }
            set
            {
                if (engine != null)
                {
                    engine.RecieveOnlyAction = value;
                }
                else
                {
                    engineNet.RecieveOnlyAction = value;
                }
            }
        }

        ~Receiver1()
        {
            Dispose(false);
        }

        private bool disposed;

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (engine != null)
                    {
                        engine.Dispose();
                    }
                    if (consumer != null)
                    {
                        consumer.Close();
                    }
                    if (service != null)
                    {
                        service = null;
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
