﻿namespace CodingByToDesign.MultiServiceBus.Communication.Contracts
{
    using System;

    interface ITransactId
    {
        long Value { get; }
    }
}
