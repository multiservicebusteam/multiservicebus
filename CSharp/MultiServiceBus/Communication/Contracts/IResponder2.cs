﻿namespace CodingByToDesign.MultiServiceBus.Communication.Contracts
{
    using System;

    public interface IResponder2<TMessageReq, TMessageRes> : IDisposable
    where TMessageReq : IMessage, new()
    where TMessageRes : IMessage, new()
    {
        Func<TMessageReq, TMessageRes> ResponseFunc { get; set; }
    }
}
