﻿namespace CodingByToDesign.MultiServiceBus.Communication.Contracts
{
    using System;

    public interface IRequester2<TMessageReq, TMessageRes> : IDisposable
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        Action<TMessageRes> ResponseAction { get; set; }
        void RequestAsync(TMessageReq message, Action<TMessageRes> responseAction = null);
        void RequestSync(TMessageReq message, Action<TMessageRes> responseAction = null);
    }
}