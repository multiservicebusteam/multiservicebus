﻿namespace CodingByToDesign.MultiServiceBus.Communication.Contracts
{
    using System;

    public interface IReceiver2<TMessageReq, TMessageRes> : IDisposable
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        Action<TransactId, TMessageReq> RecieveAction { get; set; }
        void ResponseAsync(TransactId id, TMessageRes message);
    }
}
