﻿namespace CodingByToDesign.MultiServiceBus.Communication.Contracts
{
    using System;

    public interface IReceiver1<TMessageReq> : IDisposable
    where TMessageReq : IMessage, new()
    {
        Action<TMessageReq> RecieveAction { get; set; }
    }
}
