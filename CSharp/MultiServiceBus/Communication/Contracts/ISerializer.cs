﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodingByToDesign.MultiServiceBus.Communication.Contracts
{
    internal interface ISerializer
    {
        byte[] Serialize<T>(T data);
        T Deserialize<T>(byte[] data);
    }
}
