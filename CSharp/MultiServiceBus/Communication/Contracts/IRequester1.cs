﻿namespace CodingByToDesign.MultiServiceBus.Communication.Contracts
{
    using System;

    public interface IRequester1<TMessageReq> : IDisposable
    where TMessageReq : IMessage, new()
    {
        void RequestAsync(TMessageReq message);
    }
}