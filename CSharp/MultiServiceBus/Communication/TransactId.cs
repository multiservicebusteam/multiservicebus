﻿namespace CodingByToDesign.MultiServiceBus.Communication
{
    using System;
    using System.Runtime.CompilerServices;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using System.Threading;

    public class TransactId : ITransactId
    {
        private static long reserved = 3;
        private static long seed = long.MinValue + reserved;

        public static TransactId NewTransactId()
        {
            if (seed == long.MaxValue)
            {
                seed = Interlocked.Exchange(ref seed, long.MinValue + reserved);
            }
            return new TransactId(Interlocked.Increment(ref seed));
        }

		private static TransactId empty = new TransactId(long.MinValue);

		public static TransactId Empty
		{
			get { return empty; }
		}

        private long inValue;

        public long Value { get { return inValue; } set { inValue = value; } }

        public TransactId(long seed)
        {
            this.inValue = seed;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            TransactId temp = obj as TransactId;
            if (temp == null)
                return false;
            return this.Equals(temp);
        }

        public bool Equals(TransactId value)
        {
            return this.inValue == value.inValue;
        }
    }
}