﻿namespace CodingByToDesign.MultiServiceBus.Communication
{
    using System;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal;
    using Thrift.Transport;

    public class Receiver2<TMessageReq, TMessageRes> : IReceiver2<TMessageReq, TMessageRes>, IDisposable
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        private readonly Engine<TMessageReq, TMessageRes> engine;
        private TTransport service;
        private readonly int timeout;
        private readonly EngineNetSub<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes> consumer;
        private readonly EngineNetSubTran<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes> engineNet;

        public Receiver2(int timeout = 5000, int transactionTimeout = 30000)
        {
            engine = new Engine<TMessageReq, TMessageRes>(EngineOperateKind.Receiver, timeout, transactionTimeout);
        }

        public Receiver2(string netTcpRequesterAddress, int timeout = 5000, int transactionTimeout = 30000, int bufferLength = 1000, string sessionKey = null)
        {
            if (string.IsNullOrWhiteSpace(netTcpRequesterAddress))
            {
                throw new ArgumentException("Argument is Empty", "netTcpRequesterAddress");
            }
            else
            {
                this.timeout = timeout;
                consumer = new EngineNetSub<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes>(timeout, 1000, bufferLength, sessionKey);
                consumer.Open(() => new EngineNetPub<TMessage<TMessageReq, TMessageRes>>(() => FactoryNetComm.CreateClient(netTcpRequesterAddress, out service)));

                engineNet = new EngineNetSubTran<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes>(consumer, transactionTimeout);
            }
        }

        public Action<TransactId, TMessageReq> RecieveAction
        {
            get
            {
                if (engine != null)
                {
                    return engine.RecieveAction;
                }
                else
                {
                    return engineNet.RecieveAction;
                }
            }
            set
            {
                if (engine != null)
                {
                    engine.RecieveAction = value;
                }
                else
                {
                    engineNet.RecieveAction = value;
                }
            }
        }

        public void ResponseAsync(TransactId id, TMessageRes message)
        {
            if (engine != null)
            {
                engine.Respond(id, message);
            }
            if (engineNet != null)
            {
                engineNet.ResponseAsyncOnly(id.Value, message);
            }
        }

        ~Receiver2()
        {
            Dispose(false);
        }

        private bool disposed;

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (engine != null)
                    {
                        engine.Dispose();
                    }
                    if (consumer != null)
                    {
                        consumer.Close();
                    }
                    if (service != null)
                    {
                        service = null;
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
