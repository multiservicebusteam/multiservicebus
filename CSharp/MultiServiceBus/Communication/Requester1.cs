namespace CodingByToDesign.MultiServiceBus.Communication
{
    using System;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift;
    using Thrift.Server;

    public class Requester1<TMessageReq> : IRequester1<TMessageReq>, IDisposable
    where TMessageReq : IMessage, new()
    {
        private readonly Engine<TMessageReq, NullMessage> engine;

        private readonly EngineNetPub<TMessage<TMessageReq, NullMessage>> server;
        private TServer service;
        private readonly EngineNetSub<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage> consumer;
        private readonly EngineNetSubTran<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage> engineNet;

        public Requester1(int timeout = 5000, int transactionTimeout = 30000)
        {
            engine = new Engine<TMessageReq, NullMessage>
                    (EngineOperateKind.RequesterOnly, timeout, transactionTimeout);
        }

        public Requester1(string netTcpRequesterAddress, int timeout = 5000, int transactionTimeout = 30000, int bufferLength = 1000, params string[] sessionKeys)
        {
            if (string.IsNullOrWhiteSpace(netTcpRequesterAddress))
            {
                throw new ArgumentException("Argument is Empty", "netTcpRequesterAddress"); 
            }
            else
            {
                server = new EngineNetPub<TMessage<TMessageReq, NullMessage>>(() => FactoryNetComm.CreateServer(netTcpRequesterAddress, out service), timeout, 10000000, 1000, bufferLength, sessionKeys);
                consumer = new EngineNetSub<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage>(timeout, 1000, bufferLength, null);
                consumer.Open(() => server);
                engineNet = new EngineNetSubTran<TMessage<TMessageReq, NullMessage>, TMessageReq, NullMessage>(consumer, transactionTimeout);
                engineNet.RecieveAction = (id, msg) => { };
            }
        }

        public void RequestAsync(TMessageReq message)
        {
            if (message == null)
            {
                throw new InvalidOperationException(string.Format("NoMessageDefined {0}", "message"));
            }
            if (engine != null)
            {
                var responseAction = new Action<NullMessage>(messageRespond => { });
                engine.Request(new Transact<TMessageReq>(TransactId.NewTransactId(), message), true, responseAction);
            }
            if (engineNet != null)
            {
                engineNet.RequestAsyncOnly(message);
            }
        }

        ~Requester1()
        {
            Dispose(false);
        }

        private bool disposed;

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (engine != null)
                    {
                        engine.Dispose();
                    }
                    if (service != null)
                    {
                        try
                        {
                            service.Stop();
                        }
                        catch
                        {
                        }
                        service = null;
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}