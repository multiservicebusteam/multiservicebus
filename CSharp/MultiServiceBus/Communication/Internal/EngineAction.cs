namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;

    internal class EngineAction<T> : IDisposable where T : IMessage, new()
    {
        public Action<T> Action { get; set; }
        public DateTime TimeStamp { get; set; }

        private readonly bool asyncronusly;
        private ManualResetEventSlim Waiter { get; set; }
        private bool disposed;

        public EngineAction(bool asyncronusly = true)
        {
            this.asyncronusly = asyncronusly;

            if (!asyncronusly)
                Waiter = new ManualResetEventSlim(false);
        }

        public void Set()
        {
            if (asyncronusly)
                return;

            Waiter.Set();
        }

        public void Wait(int timeout = 0)
        {
            if (asyncronusly)
                return;

            if (timeout != 0)
            {
                Waiter.Wait(timeout);
            }
            else
            {
                Waiter.Wait();
            }
        }

        ~EngineAction()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (Waiter != null)
                    {
                        Waiter.Dispose();
                        Waiter = null;
                    }
                }

                disposed = true;
            }
        }
    }
}