namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Concurrent;
    using System.Diagnostics;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;
    using CodingByToDesign.MultiServiceBus.Helpers;
    using CodingByToDesign.MultiServiceBus.ServiceLocator;

    internal class EnginePubSub<TMessageReq, TMessageRes> : IDisposable
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        private readonly EngineOperateKind psOperation;

        private readonly string psInvokerKeyRes;
        private readonly string psInvokerKeyReq;

        private IEngineInvoker<TMessageRes>[] psInvokerRequesters;
        private IEngineInvoker<TMessageReq>[] psInvokerResponders;

        EnginePubSubPubCon<TMessageReq> reqPubCon;
        EnginePubSubPubCon<TMessageRes> resPubCon;

        public EnginePubSub(
            EngineOperateKind eventOperation,
            Action<Guid, Transact<TMessageReq>, Transact<TMessageRes>> callbackInvoker)
        {
            psOperation = eventOperation;

            if (psOperation == EngineOperateKind.Requester ||
                psOperation == EngineOperateKind.RequesterOnly)
            {
                if (psOperation != EngineOperateKind.RequesterOnly)
                {
                    psInvokerKeyReq = "Res" + typeof(TMessageReq).GUID;
                    psInvokerKeyRes = "Req" + typeof(TMessageRes).GUID;
                }
                else
                {
                    psInvokerKeyReq = "Req" + typeof(TMessageRes).GUID;
                    psInvokerKeyRes = "Res" + typeof(TMessageReq).GUID;
                }

                HangInvoker();

                if (psOperation != EngineOperateKind.RequesterOnly)
                {
                    RegisterRequester(callbackInvoker);
                }

                reqPubCon = new EnginePubSubPubCon<TMessageReq>(
                    () => psInvokerResponders, LookupInvoker, psInvokerRespondersLock);

                reqPubCon.StartEngine();
            }
            else if (psOperation == EngineOperateKind.Responder ||
                     psOperation == EngineOperateKind.Receiver ||
                     psOperation == EngineOperateKind.ReceiverOnly)
            {
                if (psOperation != EngineOperateKind.Responder)
                {
                    if (psOperation == EngineOperateKind.Receiver)
                    {
                        psInvokerKeyReq = "Res" + typeof(TMessageRes).GUID;
                        psInvokerKeyRes = "Req" + typeof(TMessageReq).GUID;
                    }
                    else
                    {
                        psInvokerKeyReq = "Res" + typeof(TMessageReq).GUID;
                        psInvokerKeyRes = "Req" + typeof(TMessageRes).GUID;
                    }

                    if (psOperation != EngineOperateKind.ReceiverOnly)
                        HangInvoker();

                    RegisterReceiver(callbackInvoker);
                }
                else
                {
                    psInvokerKeyReq = "Res" + typeof(TMessageRes).GUID;
                    psInvokerKeyRes = "Req" + typeof(TMessageReq).GUID;

                    HangInvoker();

                    RegisterReceiver(callbackInvoker);
                }

                resPubCon = new EnginePubSubPubCon<TMessageRes>(
                    () => psInvokerRequesters, LookupInvoker, psInvokerRequestersLock);
                resPubCon.StartEngine();
            }
        }

        private static void RegisterRequester(Action<Guid, Transact<TMessageReq>, Transact<TMessageRes>> callbackInvoker)
        {
            ServiceLocator.Instance.RegisterMulti
            <IEngineInvoker<TMessageRes>>(
                new EngineInvoker<TMessageRes>(
                    typeof(TMessageRes).GUID,
                    (guid, data) => callbackInvoker(guid, default(Transact<TMessageReq>), data)),
                "Req" + typeof(TMessageReq).GUID);
        }

        private static void RegisterReceiver(Action<Guid, Transact<TMessageReq>, Transact<TMessageRes>> callbackInvoker)
        {
            ServiceLocator.Instance.RegisterMulti
            <IEngineInvoker<TMessageReq>>(
                new EngineInvoker<TMessageReq>(
                    typeof(TMessageReq).GUID,
                    (guid, data) => callbackInvoker(guid, data, default(Transact<TMessageRes>))),
                "Res" + typeof(TMessageReq).GUID);
        }

        public void HangInvoker()
        {
            switch (psOperation)
            {
                case EngineOperateKind.RequesterOnly:
                case EngineOperateKind.Requester:
                    LookupInvoker();
                    ServiceLocator.Instance.ServiceRegistered += ServiceRegisteredRequester;
                    break;
                case EngineOperateKind.Receiver:
                case EngineOperateKind.Responder:
                    LookupInvoker();
                    ServiceLocator.Instance.ServiceRegistered += ServiceRegisteredReceiver;
                    break;
            }
        }

        readonly ReaderWriterLockSlim psInvokerRequestersLock = new ReaderWriterLockSlim();
        readonly ReaderWriterLockSlim psInvokerRespondersLock = new ReaderWriterLockSlim();

        public bool LookupInvoker()
        {
            switch (psOperation)
            {
                case EngineOperateKind.Requester:
                    {
                        var arg = ServiceLocator.Instance.GetServices<IEngineInvoker<TMessageReq>>(psInvokerKeyReq);
                        if (arg != null && arg.Length != 0)
                        {
                            psInvokerRespondersLock.SafeWriteAction(() => psInvokerResponders = arg);
                            return true;
                        }
                    }
                    break;
                case EngineOperateKind.RequesterOnly:
                    {
                        var arg = ServiceLocator.Instance.GetServices<IEngineInvoker<TMessageReq>>(psInvokerKeyRes);
                        if (arg != null && arg.Length != 0)
                        {
                            psInvokerRespondersLock.SafeWriteAction(() => psInvokerResponders = arg);
                            return true;
                        }
                    }
                    break;
                case EngineOperateKind.Responder:
                    {
                        var arg = ServiceLocator.Instance.GetServices<IEngineInvoker<TMessageRes>>(psInvokerKeyReq);
                        if (arg != null && arg.Length != 0)
                        {
                            psInvokerRequestersLock.SafeWriteAction(() => psInvokerRequesters = arg);
                            return true;
                        }
                    }
                    {
                        var arg = ServiceLocator.Instance.GetServices<IEngineInvoker<TMessageRes>>(psInvokerKeyRes);
                        if (arg != null && arg.Length != 0)
                        {
                            psInvokerRequestersLock.SafeWriteAction(() => psInvokerRequesters = arg);
                            return true;
                        }
                    }
                    break;
                case EngineOperateKind.Receiver:
                    {
                        var arg = ServiceLocator.Instance.GetServices<IEngineInvoker<TMessageRes>>(psInvokerKeyRes);
                        if (arg != null && arg.Length != 0)
                        {
                            psInvokerRequestersLock.SafeWriteAction(() => psInvokerRequesters = arg);
                            return true;
                        }
                    }
                    break;
            }

            return false;
        }

        private void ServiceRegisteredReceiver(object sender, string key)
        {
            if (key != psInvokerKeyReq)
                return;

            var args = ServiceLocator.Instance.GetServices<IEngineInvoker<TMessageRes>>(psInvokerKeyReq);
            if (args != null && args.Length != 0)
            {
                psInvokerRequestersLock.SafeWriteAction(() => psInvokerRequesters = args);
            }
            ServiceLocator.Instance.HangStateChangedMessage<IEngineInvoker<TMessageRes>>(psInvokerKeyReq, RefreshReceiverServices);
        }

        private void ServiceRegisteredRequester(object sender, string key)
        {
            if (key != psInvokerKeyRes)
                return;

            var args = ServiceLocator.Instance.GetServices<IEngineInvoker<TMessageReq>>(psInvokerKeyRes);
            if (args != null && args.Length != 0)
            {
                psInvokerRespondersLock.SafeWriteAction(() => psInvokerResponders = args);
            }
            ServiceLocator.Instance.HangStateChangedMessage<IEngineInvoker<TMessageReq>>(psInvokerKeyRes, RefreshRequesterServices);
        }

        private void RefreshReceiverServices(IEngineInvoker<TMessageRes>[] args)
        {
            if (args != null && args.Length != 0)
            {
                psInvokerRequestersLock.SafeWriteAction(() => psInvokerRequesters = args);
            }
        }

        private void RefreshRequesterServices(IEngineInvoker<TMessageReq>[] args)
        {
            if (args != null && args.Length != 0)
            {
                psInvokerRespondersLock.SafeWriteAction(() => psInvokerResponders = args);
            }
        }

        internal void Publish(Transact<TMessageRes> message)
        {
            resPubCon.Publish(message);
        }

        internal void Publish(Transact<TMessageReq> message)
        {
            reqPubCon.Publish(message);
        }

        public void Dispose()
        {
            if (reqPubCon != null)
            {
                reqPubCon.Dispose();
            }
            if (resPubCon != null)
            {
                resPubCon.Dispose();
            }
        }
    }
}