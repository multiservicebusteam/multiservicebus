namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;

    internal class EngineInvoker<TMessage>
        : IEngineInvoker<TMessage>
        where TMessage : IMessage, new()
    {
        private readonly Guid id;
        private readonly Action<Guid, Transact<TMessage>> callback;

        public EngineInvoker(Guid id, Action<Guid, Transact<TMessage>> callback)
        {
            this.id = id;
            this.callback = callback;
        }

        public void Invoke(Transact<TMessage> arg)
        {
            this.callback(id, arg);
        }
    }
}