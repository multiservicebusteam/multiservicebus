﻿namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Threading;
    using Thrift.Protocol;
    using Thrift.Server;
    using Thrift.Transport;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift;

    class FactoryNetComm
    {
        public static ThriftService.Iface CreateServer(string netTcpClientAddress, out TServer service)
        {
            service = null;
            try
            {
                Uri uri;
                if (!Uri.TryCreate(netTcpClientAddress, UriKind.RelativeOrAbsolute, out uri)) return null;
                var handler = new ThriftServiceImpl();
                var processor = new ThriftService.Processor(handler);
                TServerTransport serverTransport = new TServerSocket(uri.Port, 1000000, true);
                TServer server = new TThreadPoolServer(processor, serverTransport);
                service = server;
                new Thread(server.Serve){IsBackground = true}.Start();
                return handler;
            }
            catch
            {
                return null;
            }
        }

        public static ThriftService.Iface CreateClient(string netTcpServerAddress, out TTransport service)
        {
            service = null;
            try
            {
                Uri uri;
                if (!Uri.TryCreate(netTcpServerAddress, UriKind.RelativeOrAbsolute, out uri)) return null;
                TSocket socket = new TSocket(uri.Host, uri.Port);
                socket.Timeout = 1000000;
                socket.TcpClient.NoDelay = true;
                var transport = new TBufferedTransport(socket, 1000000);
                TProtocol protocol = new TBinaryProtocol(transport);
                var client = new ThriftService.Client(protocol);
                service = transport;
                transport.Open();
                return client;
            }
            catch
            {
                return null;
            }
        }
    }
}