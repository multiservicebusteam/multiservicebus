namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Concurrent;
    using System.Diagnostics;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;
    using CodingByToDesign.MultiServiceBus.Helpers;

    internal class EnginePubSubPubCon<TMessage>
        where TMessage : IMessage, new()
    {
        readonly ConcurrentQueue<Transact<TMessage>> messageQueue = new ConcurrentQueue<Transact<TMessage>>();
        readonly ManualResetEventSlim messageWaiter = new ManualResetEventSlim(false);
        readonly Func<IEngineInvoker<TMessage>[]> responders;
        readonly Func<bool> respondersLookup;
        readonly ReaderWriterLockSlim locker;

        public EnginePubSubPubCon(Func<IEngineInvoker<TMessage>[]> responders, Func<bool> respondersLookup, ReaderWriterLockSlim locker)
        {
            this.responders = responders;
            this.respondersLookup = respondersLookup;
            this.locker = locker;
        }

        public void Publish(Transact<TMessage> reqMessage)
        {
            AddToMessageQueue(reqMessage);
        }

        private void AddToMessageQueue(Transact<TMessage> reqMessage)
        {
            messageQueue.Enqueue(reqMessage);
            if (!messageWaiter.IsSet)
                messageWaiter.Set();
        }

        bool workingConsume;

        public void StartEngine()
        {
            workingConsume = true;

            new Thread(
                () =>
                {
                    var invoker = responders.Invoke();

                    while (workingConsume)
                    {
                        messageWaiter.Wait(1000);

                        while (!messageQueue.IsEmpty)
                        {
                            Transact<TMessage> message;
                            if (messageQueue.TryDequeue(out message))
                            {
                                RETRY:
                                if (invoker != null)
                                {
                                    try
                                    {
                                        locker.SafeReadAction(() => invoker = responders.Invoke());
                                        for (var i = 0; i < invoker.Length; i++)
                                            invoker[i].Invoke(message);
                                    }
                                    catch (Exception exception)
                                    {
                                        Trace.WriteLine(string.Format("Exception in {0}<{1}>: {3}",
                                            "EnginePubSubPubCon",
                                            typeof(TMessage).Name,
                                            exception));
                                    }
                                }
                                else
                                {
                                    Thread.Sleep(100);
                                    respondersLookup.Invoke();
                                    locker.SafeReadAction(() => invoker = responders.Invoke());

                                    goto RETRY;
                                }
                            }
                        }
                        messageWaiter.Reset();
                    }
                }) { IsBackground = true }.Start();
        }

        public void Dispose()
        {
            workingConsume = false;
        }
    }
}