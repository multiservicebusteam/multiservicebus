namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    internal enum EngineOperateKind : short
    {
        RequesterOnly,
        Requester,
        ReceiverOnly,
        Receiver,
        Responder,
    }
}