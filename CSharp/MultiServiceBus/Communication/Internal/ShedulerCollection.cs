namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class ShedulerCollection<T> : SortedList<DateTime, List<T>>
    {
        readonly int maxCount;

        internal ShedulerCollection()
        {
            maxCount = 0;
        }

        internal ShedulerCollection(int maxCount = 0)
        {
            this.maxCount = maxCount;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool Add(DateTime key, T value)
        {
            if (maxCount > 0 && Count > maxCount)
                return false;

            if (!base.ContainsKey(key))
                base[key] = new List<T>();

            base[key].Add(value);
            return true;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public new bool Remove(DateTime key)
        {
            return base.Remove(key);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool Remove(T value)
        {
            var keys = new List<DateTime>();
            bool removedItem = false;

            foreach (var item in this)
            {
                if (item.Value.Contains(value))
                {
                    item.Value.Remove(value);
                    removedItem = true;
                    if (item.Value.Count == 0)
                        keys.Add(item.Key);
                }
            }

            foreach (var item in keys)
                base.Remove(item);

            return removedItem || keys.Count > 0;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public List<KeyValuePair<DateTime, List<T>>> Collect(DateTime now)
        {
            var values = new List<KeyValuePair<DateTime, List<T>>>();

            foreach (var value in this)
            {
                if (value.Key > now)
                    break;

                values.Add(value);
            }

            foreach (var value in values)
                Remove(value.Key);

            return values;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public TimeSpan NextSheduler(DateTime now)
        {
            foreach (var item in this)
            {
                var timeSpan = item.Key - now;
                if (timeSpan.TotalMilliseconds > 4294967293)
                    return TimeSpan.FromMilliseconds(4294967293);

                return timeSpan;
            }

            return TimeSpan.Zero;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool IsEmpty()
        {
            return Count == 0;
        }
    }
}