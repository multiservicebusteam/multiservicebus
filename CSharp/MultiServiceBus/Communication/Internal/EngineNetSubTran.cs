﻿namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication;

    class EngineNetSubTran<TMessage, TMessageReq, TMessageRes>
        where TMessage : TMessage<TMessageReq, TMessageRes>, new()
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        private readonly EngineNetSub<TMessage, TMessageReq, TMessageRes> consumer;
        private readonly int transactionTimeout;
        Sheduler<long> transactionCollector;

        public EngineNetSubTran(EngineNetSub<TMessage, TMessageReq, TMessageRes> consumer, int transactionTimeout)
        {
            this.consumer = consumer;
            this.transactionTimeout = transactionTimeout;
            this.transactionCollector = new Sheduler<long>();
            this.transactionCollector.InvokeShedulerEnd += TransactionCollectorInvokeShedulerEnd;
        }

        void TransactionCollectorInvokeShedulerEnd(DateTime durationEnd, long idReq)
        {
            if (this.consumer.Transactions.ContainsKey(idReq))
            {
                this.consumer.Transactions.Remove(idReq);
            }
        }

        public Action<TMessageRes> ResponseAction { get; set; }

        public void RequestSync(TMessageReq message, Action<TMessageRes> responseAction = null)
        {
            if (responseAction == null)
            {
                responseAction = ResponseAction;
            }
            var id = TransactId.NewTransactId().Value;
            var rid = TransactId.Empty.Value;
            var res = default(TMessageRes);
            if (consumer != null)
            {
                transactionCollector.AddSheduler(DateTime.UtcNow.AddMilliseconds(transactionTimeout), id);
                consumer.Transactions.Add(id, new Action<TMessage>(
                    (msg) =>
                    {
                          rid = msg.IdReq;
                          res = msg.MessageRes;
                    }
                ));
                consumer.Publish(new TMessage { IdReq = id, MessageReq = message });
                SpinWait.SpinUntil(() => id == rid, transactionTimeout);
                if (responseAction != null)
                {
                    responseAction(res);
                }
            }
        }

        public void RequestAsync(TMessageReq message, Action<TMessageRes> responseAction = null)
        {
            if (responseAction == null)
            {
                responseAction = ResponseAction;
            }
            var id = TransactId.NewTransactId().Value;
            var rid = TransactId.Empty.Value;
            var res = default(TMessageRes);
            if (consumer != null)
            {
                transactionCollector.AddSheduler(DateTime.UtcNow.AddMilliseconds(transactionTimeout), id);
                consumer.Transactions.Add(id, new Action<TMessage>(
                    (msg) =>
                    {
                        rid = msg.IdReq;
                        res = msg.MessageRes;
                        if (responseAction != null)
                        {
                            responseAction(res);
                        }
                    }
                ));
                consumer.Publish(new TMessage { IdReq = id, MessageReq = message });
            }
        }

        public void RequestAsyncOnly(TMessageReq message)
        {
            var id = TransactId.Empty.Value + 1;
            if (consumer != null)
            {
                consumer.Publish(new TMessage{ IdReq = id, MessageReq = message });
            }
        }

        public void ResponseAsyncOnly(long id, TMessageRes message)
        {
            if (consumer != null)
            {
                consumer.Publish(new TMessage{ IdReq = id, MessageRes = message });
            }
        }

        public Action<TMessageReq> RecieveOnlyAction
        {
            get
            {
                return new Action<TMessageReq>((msg) => consumer.RecivedAction(new TMessage { MessageReq = msg }));
            }
            set
            {
                consumer.RecivedAction = new Action<TMessage>((msg) => value(msg.MessageReq));
            }
        }
        
        public Action<TransactId, TMessageReq> RecieveAction
        {
            get
            {
                return new Action<TransactId, TMessageReq>
                    ((id, msg) => consumer.RecivedAction(new TMessage{ IdReq = id.Value, MessageReq = msg }));
            }
            set
            {
                consumer.RecivedAction = new Action<TMessage>((msg) => value(new TransactId(msg.IdReq), msg.MessageReq));
            }
        }

        public Func<TMessageReq, TMessageRes> ResponseFunc {
            get
            {
                return new Func<TMessageReq, TMessageRes>(
                    (req) =>
                    {
                        var action = consumer.RecivedAction;
                        var message = (TMessage)new TMessage<TMessageReq, TMessageRes>{MessageReq = req};
                        action(message);
                        return message.MessageRes;
                    }
                );
            }
            set
            {
                consumer.RecivedAction =
                    new Action<TMessage>(
                        (msg) =>
                        {
                            msg.MessageRes = value(msg.MessageReq);
                            msg.MessageReq = default(TMessageReq);
                            consumer.Publish(msg);
                        }
                    );
            }
        }
    }
}
