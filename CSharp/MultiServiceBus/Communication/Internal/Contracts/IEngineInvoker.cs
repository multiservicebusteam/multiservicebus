namespace CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts
{
    using System;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;

    internal interface IEngineInvoker<TMessage>
        where TMessage : IMessage, new()
    {
        void Invoke(Transact<TMessage> arg);
    }
}