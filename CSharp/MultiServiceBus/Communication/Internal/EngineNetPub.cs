﻿namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift;
    using System.Diagnostics;
    using CodingByToDesign.MultiServiceBus.Communication.Internal;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;

    class EngineNetPub<T>
    {
        ThriftService.Iface channel;
        readonly Func<ThriftService.Iface> channelFactory;

        readonly int timeout;
        readonly int maxBufferLength;
        readonly int maxConsumeLength;
        readonly int bufferLength;

        public EngineNetPub(Func<ThriftService.Iface> channelFactory, int timeout = 30000, int maxBufferLength = 1000000, int maxConsumeLength = 1000, int bufferLength = 1000, params string[] sessionKeys)
        {
            this.channelFactory = channelFactory;
            this.timeout = timeout;
            this.maxBufferLength = maxBufferLength;
            this.maxConsumeLength = maxConsumeLength;
            this.bufferLength = bufferLength;
            if (sessionKeys != null)
            {
                foreach (var sessionKey in sessionKeys)
                {
                    BeginSession(sessionKey);
                }
            }
        }

        private ThriftService.Iface CreateChannel()
        {
            if (channel == null)
                channel = channelFactory.Invoke();
            return channel;
        }

        public string BeginSession(string sessionKey)
        {
            CreateChannel();
            return channel.BeginSession(sessionKey);
        }

        ISerializer serializer = new Serializer();

        public void Publish(string sessionKey = null, params T[] messages)
        {
            CreateChannel();
            var data = new List<ThriftMessage>();
            foreach (var message in messages)
            {
                data.Add(new ThriftMessage { SerializedMessage = serializer.Serialize(message) });
            }
            channel.Publish(sessionKey, data);
        }

        public T[] Consume(string sessionKey)
        {
            CreateChannel();
            var messages = channel.Consume(sessionKey);
            var data = new List<T>();
            foreach (var message in messages)
            {
                data.Add(serializer.Deserialize<T>(message.SerializedMessage));
            }
            return data.ToArray();
        }

        public bool EndSession(string sessionKey)
        {
            CreateChannel();
            return channel.EndSession(sessionKey);
        }
    }
}