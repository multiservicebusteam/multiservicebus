﻿namespace CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Threading;

    internal class ThriftServiceImpl
      : ThriftService.Iface
    {
        readonly int _timeout;
        readonly int _maxBufferLength;
        readonly int _maxConsumeLength;
        readonly IDictionary<string, ConcurrentQueue<ThriftMessage>> _notifications;

        public ThriftServiceImpl(int timeout = 5000, int maxBufferLength = 1000000, int maxConsumeLength = 1000, params string[] sessionKeys)
        {
            _notifications = new ConcurrentDictionary<string, ConcurrentQueue<ThriftMessage>>();
            _timeout = timeout;
            _maxBufferLength = maxBufferLength;
            _maxConsumeLength = maxConsumeLength;
			foreach(var sessionKey in sessionKeys)
            {
                BeginSession(sessionKey);
            }
        }

        public string BeginSession(string sessionKey)
        {
            var key = string.IsNullOrEmpty(sessionKey) ? Guid.NewGuid().ToString() : sessionKey;
            if (!_notifications.ContainsKey(key))
            {
                _notifications.Add(key, new ConcurrentQueue<ThriftMessage>());
            }
            return key;
        }

        public void Publish(string sessionKey, List<ThriftMessage> notifications)
        {
            if (notifications == null) return;
            foreach (var queue in _notifications)
            {
                if (queue.Key == sessionKey) continue;
                if (queue.Value.Count > _maxBufferLength)
                    _notifications.Remove(queue.Key);
                else
                    foreach (var notification in notifications)
                        queue.Value.Enqueue(notification);
            }
        }

        public List<ThriftMessage> Consume(string sessionKey)
        {
            var values = new List<ThriftMessage>();
            if (SpinWait.SpinUntil(() => _notifications.ContainsKey(sessionKey), _timeout))
            {
                ConcurrentQueue<ThriftMessage> queue;
                if (_notifications.TryGetValue(sessionKey, out queue))
                {
                    if (SpinWait.SpinUntil(() => !queue.IsEmpty, _timeout))
                    {
                        int maxLength = _maxConsumeLength;
                        while (!queue.IsEmpty)
                        {
                            ThriftMessage value;
                            if (queue.TryDequeue(out value))
                                values.Add(value);
                            if (--maxLength == 0)
                                break;
                        }
                        return values;
                    }
                }
            }
            return values;
        }

        public bool EndSession(string sessionKey)
        {
            return _notifications.Remove(sessionKey);
        }
    }
}
