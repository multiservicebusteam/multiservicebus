﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CodingByToDesign.MultiServiceBus.Communication.Contracts;

namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    internal class Serializer : ISerializer
    {
        public byte[] Serialize<T>(T data)
        {
            using (var stream = new MemoryStream())
            {
                ProtoBuf.Serializer.Serialize<T>(stream, data);
                return stream.ToArray();
            }
        }

        public T Deserialize<T>(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                return ProtoBuf.Serializer.Deserialize<T>(stream);
            }
        }
    }
}
