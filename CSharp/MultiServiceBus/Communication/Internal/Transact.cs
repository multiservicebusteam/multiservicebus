﻿namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;

    internal class Transact<TMessage>
    where TMessage : IMessage
    {
        internal Transact(TransactId id, TMessage message)
        {
            Id = id;
            Message = message;
        }

        internal TransactId Id;
        internal TMessage Message;
    }
}
