namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;

    internal class Engine<TMessageReq, TMessageRes> : IDisposable
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        int defaultTimeout = 5000;
        int defaultTransactionsTimeout = 30000;

        readonly EngineOperateKind engineOperateKind;
        readonly EnginePubSub<TMessageReq, TMessageRes> enginePubSub;
        readonly Guid engineSubGuid;

        readonly IDictionary<TransactId, EngineAction<TMessageRes>> engineInProgressTransacts;

        Func<TMessageReq, TMessageRes> engineResponseFunc;
        Action<TMessageReq> engineReceiveOnlyAction;
        Action<TransactId, TMessageReq> engineReceiveAction;

        public Func<TMessageReq, TMessageRes> ResponseFunc
        {
            get { return engineResponseFunc; }
            set
            {
                if (engineOperateKind != EngineOperateKind.Responder)
                {
                    throw new InvalidOperationException("NoResponder");
                }

                engineResponseFunc = value;
            }
        }

        public Action<TMessageReq> RecieveOnlyAction
        {
            get { return engineReceiveOnlyAction; }
            set
            {
                if (engineOperateKind != EngineOperateKind.ReceiverOnly)
                {
                    throw new InvalidOperationException("NoReceiverOnly");
                }

                engineReceiveOnlyAction = value;
            }
        }

        public Action<TransactId, TMessageReq> RecieveAction
        {
            get { return engineReceiveAction; }
            set
            {
                if (engineOperateKind != EngineOperateKind.Receiver)
                {
                    throw new InvalidOperationException("NoReceiver");
                }

                engineReceiveAction = value;
            }
        }

        public Engine(EngineOperateKind eventOperation, int defaultTimeout = 5000, int defaultTransactionsTimeout = 30000)
        {
            this.defaultTimeout = defaultTimeout;
            this.defaultTransactionsTimeout = defaultTransactionsTimeout;

            engineOperateKind = eventOperation;
            if (engineOperateKind == EngineOperateKind.Requester ||
                engineOperateKind == EngineOperateKind.RequesterOnly)
            {
                engineSubGuid = typeof(TMessageRes).GUID;
            }
            else if (engineOperateKind == EngineOperateKind.Responder ||
                     engineOperateKind == EngineOperateKind.Receiver ||
                     engineOperateKind == EngineOperateKind.ReceiverOnly)
            {
                engineSubGuid = typeof(TMessageReq).GUID;
            }

            engineInProgressTransacts = new ConcurrentDictionary<TransactId, EngineAction<TMessageRes>>();

            enginePubSub = new EnginePubSub<TMessageReq, TMessageRes>(eventOperation, CallbackInvoke);

            if (engineOperateKind != EngineOperateKind.RequesterOnly)
                new Thread(CollectThread) { IsBackground = true }.Start();
        }

        private void CallbackInvoke(Guid messageId, Transact<TMessageReq> reqMessage, Transact<TMessageRes> resMessage)
        {
            if (!engineSubGuid.Equals(messageId))
            {
                throw new InvalidOperationException("NotCorrectCallback");
            }

            if (engineOperateKind == EngineOperateKind.Requester)
            {
                EngineAction<TMessageRes> action;

                if (engineInProgressTransacts.TryGetValue(resMessage.Id, out action))
                {
                    action.Action(resMessage.Message);
                    action.Set();
                    engineInProgressTransacts.Remove(resMessage.Id);
                }
            }
            else if (engineOperateKind == EngineOperateKind.Responder)
            {
                if (engineResponseFunc != null)
                {
                    TMessageRes resData = engineResponseFunc(reqMessage.Message);
                    enginePubSub.Publish(new Transact<TMessageRes>(reqMessage.Id, resData));
                }
            }
            else if (engineOperateKind == EngineOperateKind.Receiver)
            {
                if (engineReceiveAction != null)
                {
                    engineReceiveAction(reqMessage.Id, reqMessage.Message);
                }
            }
            else if (engineOperateKind == EngineOperateKind.ReceiverOnly)
            {
                if (engineReceiveOnlyAction != null)
                {
                    engineReceiveOnlyAction(reqMessage.Message);
                }
            }
        }

        public void Request(Transact<TMessageReq> message, bool asyncronusly, Action<TMessageRes> action)
        {
            if (engineOperateKind != EngineOperateKind.Requester &&
                engineOperateKind != EngineOperateKind.RequesterOnly)
            {
                throw new ArgumentException("NoRequester");
            }

            if (engineOperateKind == EngineOperateKind.RequesterOnly)
            {
                enginePubSub.Publish(message);
                return;
            }

            var transactionAction = new EngineAction<TMessageRes>(asyncronusly) { Action = action, TimeStamp = DateTime.UtcNow };

            engineInProgressTransacts.Add(message.Id, transactionAction);
            enginePubSub.Publish(message);

            transactionAction.Wait(defaultTimeout);
            transactionAction.Dispose();
        }

        public void Request(Transact<TMessageReq> message, Action<TMessageRes> action)
        {
            Request(message, false, action);
        }

        public void Respond(TransactId id, TMessageRes message)
        {
            if (engineOperateKind != EngineOperateKind.Receiver &&
                engineOperateKind != EngineOperateKind.ReceiverOnly)
            {
                throw new ArgumentException("NoReceiver");
            }

            enginePubSub.Publish(new Transact<TMessageRes>(id, message));
        }

        readonly ManualResetEventSlim collectWait = new ManualResetEventSlim(false);
        readonly ManualResetEventSlim cleanTrWait = new ManualResetEventSlim(false);
        readonly ManualResetEventSlim disposeWait = new ManualResetEventSlim(false);

        private void CollectThread()
        {
            while (!collectWait.IsSet)
            {
                cleanTrWait.Reset();

                var now = DateTime.UtcNow;

                foreach (var transaction in engineInProgressTransacts)
                {
                    if ((transaction.Value.TimeStamp - now).TotalMilliseconds > defaultTransactionsTimeout)
                    {
                        engineInProgressTransacts.Remove(transaction.Key);
                        transaction.Value.Dispose();
                    }
                }

                cleanTrWait.Wait(defaultTransactionsTimeout);
            }

            disposeWait.Set();
        }

        void CleanInProgressTransacts()
        {
            KeyValuePair<TransactId, EngineAction<TMessageRes>> transactionForRemove;

            while (engineInProgressTransacts.Count != 0)
            {
                transactionForRemove = engineInProgressTransacts.FirstOrDefault();

				if (transactionForRemove.Key != null)
				{
					EngineAction<TMessageRes> action;
					if (engineInProgressTransacts.TryGetValue (transactionForRemove.Key, out action))
					{
						action.Dispose ();
						engineInProgressTransacts.Remove (transactionForRemove.Key);
					}
				}
            }
        }

        public void Dispose()
        {
            engineReceiveAction = null;
            engineResponseFunc = null;

            CleanInProgressTransacts();

            if (engineOperateKind != EngineOperateKind.RequesterOnly)
            {
                collectWait.Set();
                cleanTrWait.Set();
                disposeWait.Wait();

                collectWait.Dispose();
                cleanTrWait.Dispose();
                disposeWait.Dispose();

                enginePubSub.Dispose();
            }
        }
    }
}