﻿namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using ProtoBuf;

    [Serializable]
    [ProtoContract]
    internal class TMessage<TMessageReq, TMessageRes> : IMessage
        where TMessageReq : IMessage
        where TMessageRes : IMessage
    {
        public TMessage() { IdReq = long.MinValue; }
        [ProtoMember(1000)]
        public long IdReq { get; set; }
        [ProtoMember(2000)]
        public TMessageReq MessageReq { get; set; }
        [ProtoMember(3000)]
        public TMessageRes MessageRes { get; set; }
    }
}
