namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using ProtoBuf;

    [Serializable]
    [ProtoContract]
    public class NullMessage : IMessage
    {
    }
}
