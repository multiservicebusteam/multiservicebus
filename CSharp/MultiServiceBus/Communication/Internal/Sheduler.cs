namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Threading.Tasks;

    internal class Sheduler<T> : IDisposable
    {
        private const long MaxSheduler = 4294967293;
        private readonly long minSheduler;

        private ShedulerCollection<T> durations;
        private readonly Timer timer;
        private bool disposed;

        public Sheduler()
        {
            this.minSheduler = 2;
            this.durations = new ShedulerCollection<T>();
            this.timer = new Timer(TimerCallback);
        }

        internal Sheduler(long minSheduler, int maxCount)
        {
            this.minSheduler = minSheduler;
            this.durations = new ShedulerCollection<T>(maxCount);
            this.timer = new Timer(TimerCallback);
        }

        public bool AddSheduler(DateTime durationEnd, T data)
        {
            if (durations.Add(durationEnd, data))
            {
                TimerChange();
                return true;
            }
            return false;
        }

        public void RemoveSheduler(T data)
        {
            if (durations.Remove(data))
                TimerChange();
        }

        private void TimerCallback(object state)
        {
            var values = durations.Collect(DateTime.UtcNow);

            foreach (var value in values)
                foreach (var data in value.Value)
                    OnInvokeShedulerEnd(value.Key, data);

            TimerChange();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void TimerChange()
        {
            if (durations.IsEmpty())
                return;

            var timeSpan = durations.NextSheduler(DateTime.UtcNow);

            if (timeSpan.TotalMilliseconds <= MaxSheduler)
            {
                if (timeSpan.TotalMilliseconds > minSheduler)
                    timer.Change(timeSpan, TimeSpan.FromMilliseconds(MaxSheduler));
                else
                    timer.Change(TimeSpan.FromMilliseconds(minSheduler), TimeSpan.FromMilliseconds(MaxSheduler));
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal DateTime Update(TimeSpan timeSpan)
        {
            try
            {
                var durations = new ShedulerCollection<T>();
                var timeStamp = DateTime.UtcNow;

                foreach (var duration in durations)
                {
                    timeStamp = duration.Key;
                    break;
                }
                if (timeSpan.Equals(TimeSpan.Zero))
                {
                    var list = new List<T>();
                    durations.Add(timeStamp, list);

                    foreach (var duration in durations)
                    {
                        list.AddRange(duration.Value);
                    }
                }
                else
                {
                    foreach (var duration in durations)
                    {
                        durations.Add(timeStamp, duration.Value);
                        timeStamp = timeStamp.Add(timeSpan);
                    }
                }

                this.durations = durations;

                return timeStamp;
            }
            finally
            {
                TimerChange();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal DateTime Next()
        {
            foreach (var duration in durations)
                return duration.Key;
            return DateTime.UtcNow;
        }

        public delegate void InvokeShedulerEventHandler(DateTime durationEnd, T data);

        public event InvokeShedulerEventHandler InvokeShedulerEndAsync;

        public event InvokeShedulerEventHandler InvokeShedulerEnd;

        private void OnInvokeShedulerEnd(DateTime durationEnd, T data)
        {
            if (InvokeShedulerEnd != null)
            {
                InvokeShedulerEnd(durationEnd, data);
            }
            else if (InvokeShedulerEndAsync != null)
            {
                new Task(() => InvokeShedulerEndAsync(durationEnd, data)).Start();
            }
        }

        public bool IsEmpty
        {
            get { return durations.IsEmpty(); }
        }

        ~Sheduler()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (timer != null)
                        timer.Dispose();
                }

                disposed = true;
            }
        }
    }
}