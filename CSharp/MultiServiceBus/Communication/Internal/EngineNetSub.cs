﻿namespace CodingByToDesign.MultiServiceBus.Communication.Internal
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;

    class EngineNetSub<TMessage, TMessageReq, TMessageRes>
        where TMessage : TMessage<TMessageReq, TMessageRes>, new()
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        class TEngineNetSub<UMessage, UMessageReq, UMessageRes>
            where UMessage : TMessage<UMessageReq, UMessageRes>, new()
            where UMessageReq : IMessage, new()
            where UMessageRes : IMessage, new()
        {
            readonly int timeout;
            readonly int retry;

            internal volatile bool working;
			internal volatile bool started;

            public TEngineNetSub(int timeout = 1000, int retry = 1000, string sessionKey = null)
            {
                this.timeout = timeout;
                this.retry = retry;
                if (!string.IsNullOrEmpty(sessionKey))
                {
                    this.sessionKey = sessionKey;
                    this.sessionKeyStatic = true;
                }
                else
                {
                    sessionKey = string.Empty;
                }
            }

            public readonly ConcurrentDictionary<long, Action<UMessage>> transactions = new ConcurrentDictionary<long, Action<UMessage>>();
            public Action<UMessage> RecivedAction { get; set; }

            private EngineNetPub<UMessage> client;
            private string sessionKey;
            private bool sessionKeyStatic = false;

            public void Open(Func<EngineNetPub<UMessage>> clientCreateFunc)
            {
                if (clientCreateFunc == null)
                    throw new ArgumentNullException("clientCreateFunc");
                client = clientCreateFunc();
                working = true;
                var addedThread = new Thread(() =>
                {
                    while (working)
                    {
                        try
                        {
                            if (sessionKey != null && !sessionKeyStatic)
                            {
                                client.EndSession(sessionKey);
                            }
                            sessionKey = client.BeginSession(sessionKey);
                            started = true;
                        }
                        catch
                        {
                            Thread.Sleep(timeout);
                            client = clientCreateFunc();
                            continue;
                        }
                        var maxErrors = retry;
                        while (working)
                        {
                            try
                            {
                                var notifications = client.Consume(sessionKey);
                                maxErrors = retry;
                                if (notifications.Length != 0)
                                {
                                    foreach (var newValue in notifications)
                                    {
                                        Action<UMessage> received;
                                        if (!transactions.IsEmpty &&
                                            transactions.ContainsKey(newValue.IdReq) &&
                                            transactions.TryRemove(newValue.IdReq, out received))
                                        {
                                            received.Invoke(newValue);
                                        }
                                        else
                                        {
                                            received = RecivedAction;
                                            if (received != null)
                                            {
                                                received.Invoke(newValue);
                                            }
                                        }
                                    }
                                }
                            }
                            catch
                            {
                                if (--maxErrors < 0)
                                    break;
                                Thread.Sleep(timeout);
                                client = clientCreateFunc();
                            }
                        }
                        try
                        {
                            client.EndSession(sessionKey);
                        }
                        catch { }
                    }
                });
                addedThread.IsBackground = true;
                addedThread.Start();
            }

            public void Publish(params UMessage[] messages)
            {
                var c = client;
                var k = sessionKey;
                if (c == null || string.IsNullOrEmpty(k)) return;
                c.Publish(k, messages);
            }

            public void Close()
            {
                working = false;
            }
        }

        readonly int timeout = 0;
        readonly int bufferLength = 0;
        readonly TEngineNetSub<TMessage, TMessageReq, TMessageRes> consumer;

        public EngineNetSub(int timeout = 1000, int retry = 1000, int bufferLength = 1000, string sessionKey = null)
        {
            this.timeout = timeout;
            this.bufferLength = bufferLength;
            consumer = new TEngineNetSub<TMessage, TMessageReq, TMessageRes>(timeout, retry, sessionKey);
        }

        public IDictionary<long, Action<TMessage>> Transactions
        {
            get { return consumer.transactions; }
        }

        public Action<TMessage> RecivedAction
        {
            get { return consumer.RecivedAction; }
            set { consumer.RecivedAction = value; }
        }

        ConcurrentQueue<TMessage> messages = new ConcurrentQueue<TMessage>();
        private bool working = true;

        public void Open(Func<EngineNetPub<TMessage>> clientCreateFunc)
        {
            consumer.Open(clientCreateFunc);
            OpenConsumerPublishThread();
        }

        private void OpenConsumerPublishThread()
        {
            new Thread(() =>
            {
                while (working)
                {
                    if (SpinWait.SpinUntil(() => !messages.IsEmpty, timeout))
                    {
                        var buffer = new List<TMessage>(bufferLength);
                        while (working && !messages.IsEmpty && buffer.Count < bufferLength)
                        {
                            TMessage message;
                            if (messages.TryDequeue(out message))
                            {
                                buffer.Add(message);
                            }
                        }
                        if (working)
                        {
                            consumer.Publish(buffer.ToArray());
                        }
                    }
                }
            }) { IsBackground = true }.Start();
        }

        public void Publish(TMessage message)
        {
            consumer.Publish(message);
        }

        public void Close()
        {
            working = false;
            consumer.Close();
        }
    }
}