﻿namespace CodingByToDesign.MultiServiceBus.Communication
{
    using System;
    using System.Threading;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication.Internal;
    using CodingByToDesign.MultiServiceBus.Communication.Internal.CThrift;
    using Thrift.Server;

    public class Requester2<TMessageReq, TMessageRes> : IRequester2<TMessageReq, TMessageRes>, IDisposable
        where TMessageReq : IMessage, new()
        where TMessageRes : IMessage, new()
    {
        private Engine<TMessageReq, TMessageRes> engine;

        private TServer service;
        private readonly EngineNetPub<TMessage<TMessageReq, TMessageRes>> server;
        private readonly EngineNetSub<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes> consumer;
        private readonly EngineNetSubTran<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes> engineNet;

        public Requester2(int timeout = 5000, int transactionTimeout = 30000)
        {
            engine = new Engine<TMessageReq, TMessageRes>(EngineOperateKind.Requester, timeout, transactionTimeout);
            ResponseAction = null;
        }

        public Requester2(string netTcpRequesterAddress, int timeout = 5000, int transactionTimeout = 30000, int bufferLength = 1000, params string[] sessionKeys)
        {
            if (string.IsNullOrWhiteSpace(netTcpRequesterAddress))
            {
                throw new ArgumentException("Argument is Empty", "netTcpRequesterAddress");
            }
            else
            {
                server = new EngineNetPub<TMessage<TMessageReq, TMessageRes>>(() => FactoryNetComm.CreateServer(netTcpRequesterAddress, out service), timeout, 10000000, 1000, bufferLength, sessionKeys);

                consumer = new EngineNetSub<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes>(timeout, 1000, bufferLength, null);
                consumer.Open(() => server);

                engineNet = new EngineNetSubTran<TMessage<TMessageReq, TMessageRes>, TMessageReq, TMessageRes>(consumer, transactionTimeout);
            }
        }

        Action<TMessageRes> responseAction;

        public Action<TMessageRes> ResponseAction
        {
            get
            {
                if (engine != null)
                {
                    return responseAction;
                }
                else
                {
                    return engineNet.ResponseAction;
                }
            }
            set
            {
                if (engine != null)
                {
                    responseAction = value;
                }
                else
                {
                    engineNet.ResponseAction = value;
                }
            }
        }

        public void RequestSync(TMessageReq message, Action<TMessageRes> responseAction = null)
        {
            if (message == null)
            {
                throw new InvalidOperationException(string.Format("NoMessageDefined {0}", "message"));
            }

            if (responseAction == null)
            {
                responseAction = ResponseAction;
            }

            if (responseAction == null)
            {
                throw new InvalidOperationException(string.Format("NoActionDefined action {0}", "responseAction"));
            }

            if (engine != null)
            {
                engine.Request(new Transact<TMessageReq>(TransactId.NewTransactId(), message), false, responseAction);
            }

            if (engineNet != null)
            {
                engineNet.RequestSync(message, responseAction);
            }
        }

        public void RequestAsync(TMessageReq message, Action<TMessageRes> responseAction = null)
        {
            if (message == null)
            {
                throw new InvalidOperationException(string.Format("NoMessageDefined {0}", "message"));
            }

            if (responseAction == null)
            {
                responseAction = ResponseAction;
            }

            if (responseAction == null)
            {
                throw new InvalidOperationException(string.Format("NoActionDefined action {0}", "responseAction"));
            }

            if (engine != null)
            {
                engine.Request(new Transact<TMessageReq>(TransactId.NewTransactId(), message), true, responseAction);
            }

            if (engineNet != null)
            {
                engineNet.RequestAsync(message, responseAction);
            }
        }

        ~Requester2()
        {
            Dispose(false);
        }

        private bool disposed;

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (engine != null)
                    {
                        engine.Dispose();
                    }
                    if (consumer != null)
                    {
                        consumer.Close();
                    }
                    if (service != null)
                    {
                        try
                        {
                            service.Stop();
                        }
                        catch
                        {
                        }
                        service = null;
                    }
                }

                engine = null;
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}