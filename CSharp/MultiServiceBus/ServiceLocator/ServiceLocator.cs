namespace CodingByToDesign.MultiServiceBus.ServiceLocator
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using CodingByToDesign.MultiServiceBus.ServiceLocator.Internal;

    public class ServiceLocator
    {
        static readonly ServiceLocator instance = new ServiceLocator();

        public static ServiceLocator Instance
        {
            get { return instance; }
        }

        internal IDictionary<string, ServiceLocatorStore> Services { get; private set; }

        private ServiceLocator()
        {
            Services = new ConcurrentDictionary<string, ServiceLocatorStore>();
        }

        public delegate void ServiceRegisteredEventHandler(object sender, string key);
        public event ServiceRegisteredEventHandler ServiceRegistered;

        public bool Register<T>(T service, string key = null)
        {
            string serviceKey = key ?? string.Empty + typeof(T).GUID;
            ServiceLocatorStore store;
            if (Services.TryGetValue(serviceKey, out store))
            {
                store.Replace(serviceKey, service);
            }
            else
            {
                store = new ServiceLocatorStore(serviceKey, service);
                Services.Add(serviceKey, store);
            }
            if (ServiceRegistered != null)
            {
                ServiceRegistered(this, serviceKey);
            }
            return Services.ContainsKey(serviceKey);
        }

        public bool RegisterMulti<T>(T service, string key = null)
        {
            string serviceKey = key ?? string.Empty + typeof(T).GUID;
            ServiceLocatorStore store;
            if (Services.TryGetValue(serviceKey, out store))
            {
                store.AddService(serviceKey, service);
            }
            else
            {
                store = new ServiceLocatorStore(serviceKey, service);
                Services.Add(serviceKey, store);
            }
            if (ServiceRegistered != null)
            {
                ServiceRegistered(this, serviceKey);
            }
            return Services.ContainsKey(serviceKey);
        }

        public bool HangStateChangedMessage<T>(string key, Action<T[]> action)
        {
            string serviceKey = key ?? string.Empty + typeof(T).GUID;
            ServiceLocatorStore store;
            if (Services.TryGetValue(serviceKey, out store))
            {
                store.StoreChanged += (sender, services) =>
                {
                    var servicesT = new List<T>();
                    for (var i = 0; i < services.Length; i++)
                    {
                        if (services[i] is T)
                        {
                            servicesT.Add((T)services[i]);
                        }
                    }
                    action(servicesT.ToArray());
                };
                return true;
            }
            return false;
        }

        public T GetService<T>(string key = null)
        {
            string serviceKey = key ?? string.Empty + typeof(T).GUID;
            ServiceLocatorStore store;
            if (Services.TryGetValue(serviceKey, out store))
            {
                var services = store.Store.ToArray();
                if (services[0] is T)
                {
                    return (T)services[0];
                }
            }
            return default(T);
        }

        public T[] GetServices<T>(string key = null)
        {
            string serviceKey = key ?? string.Empty + typeof(T).GUID;
            ServiceLocatorStore store;
            if (Services.TryGetValue(serviceKey, out store))
            {
                var services = store.Store.ToArray();
                var servicesT = new List<T>();
                for (var i = 0; i < services.Length; i++)
                {
                    if (services[i] is T)
                    {
                        servicesT.Add((T)services[i]);
                    }
                }
                return servicesT.ToArray();
            }
            return default(T[]);
        }
    }
}
