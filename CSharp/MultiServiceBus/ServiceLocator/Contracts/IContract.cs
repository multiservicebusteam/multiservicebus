namespace CodingByToDesign.MultiServiceBus.ServiceLocator
{
  public interface IContract
  {
    void Invoke();
  }

  public interface IContract<in T>
  {
    void Invoke(T arg);
  }

  public interface IContract<in T, out TResult>
  {
    TResult Invoke(T arg);
  }
}
