namespace CodingByToDesign.MultiServiceBus.ServiceLocator.Internal
{
    using System.Collections.Generic;

    internal class ServiceLocatorStore
    {
        public ServiceLocatorStore(string key, object service = null)
        {
            Key = key;
            Store = new List<object>();
            if (service != null)
            {
                Store.Add(service);
                Service = service;
            }
        }

        public string Key { get; private set; }

        public bool AddService(string serviceKey, object service)
        {
            if (Key == serviceKey)
            {
                if (service != null)
                {
                    Store.Add(service);

                    if (Service == null)
                        Service = service;

                    if (StoreChanged != null)
                    {
                        StoreChanged(this, Store.ToArray());
                    }

                    return true;
                }
            }

            return false;
        }

        internal bool Replace(string serviceKey, object service)
        {
            if (Key == serviceKey)
            {
                if (service != null)
                {
                    if (Store.Count == 0)
                        Store.Add(service);
                    else
                        Store[0] = service;
                    Service = service;

                    return true;
                }
            }

            return false;
        }

        public List<object> Store { get; private set; }

        public object Service { get; private set; }

        public delegate void StoreChangedEventHandler(object sender, object[] services);

        public event StoreChangedEventHandler StoreChanged;
    }
}