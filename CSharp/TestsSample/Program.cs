﻿namespace TestsSample
{
    using System;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using CodingByToDesign.MultiServiceBus.Communication;

    // This is definition of your message protocol
    // and you need put it in shared assembly for
    // all components that need to communicate.
    // Only requirement is to derive from IMessage,
    // thet interface is empty.
    public class MyMessageReq : IMessage
    {
        // this is example of entity content.
        public string Uri { get; set; }
        public DateTime TimeStampUtc { get; set; }
        public string HttpMethod { get; set; }
    }

    // This is exqmple response message with data.
    public class MyMessageRes : IMessage
    {
        // this is example of entity content.
        public string Data { get; set; }
    }

    class MultiServiceBusDemoClassInProc
    {
        // recevier in-proc
        private static IReceiver1<MyMessageReq> receiver1 =
            new Receiver1<MyMessageReq> { RecieveAction = ReceiveMessage };

        // receiver in-proc receive method
        private static void ReceiveMessage(MyMessageReq message)
        {
            // do what you want with received messages here
        }

        // requester in-proc
        private static IRequester1<MyMessageReq> requester1 =
            new Requester1<MyMessageReq>();

        // in-proc send/request method
        private static void SendMessageAsync1()
        {
            // sending example message to any Receiver<MyMessageReq>
            requester1.RequestAsync(new MyMessageReq
            {
                Uri = "http://codingbytodesign.net",
                TimeStampUtc = DateTime.UtcNow,
                HttpMethod = "GET"
            });
        }

        // receiver in-proc that can response but do not have to
        private static IReceiver2<MyMessageReq, MyMessageRes> receiver2 =
            new Receiver2<MyMessageReq, MyMessageRes> { RecieveAction = ReceiveMessage2 };

        // method that receive messages
        private static void ReceiveMessage2(TransactId id, MyMessageReq mesage)
        {
            // you can response back but you do not have to
            receiver2.ResponseAsync(id, new MyMessageRes { Data = "CodingByToDesign.NET" });
        }

        // requester in-proc that can get responses
        private static IRequester2<MyMessageReq, MyMessageRes> requester2 =
            new Requester2<MyMessageReq, MyMessageRes> { ResponseAction = ReceiveMessageBack2 };

        private static void ReceiveMessageBack2(MyMessageRes message)
        {
            // do what you want with received response messages here
        }

        // in-proc send/request method 2
        private static void SendMessageAsync2()
        {
            // sending example message
            requester2.RequestAsync(new MyMessageReq
            {
                Uri = "http://codingbytodesign.net",
                TimeStampUtc = DateTime.UtcNow,
                HttpMethod = "GET"
            },
                // optional parameter if it is null ReceiveMessageBack2 will be used
            new Action<MyMessageRes>(
                msgRes => { /* use your response message here */ }));
        }

        // responder in-proc that have to respond always for messages
        private static IResponder2<MyMessageReq, MyMessageRes> responder3 =
            new Responder2<MyMessageReq, MyMessageRes> { ResponseFunc = RespondMessage };

        // method that receive messages and need to create respond message
        private static MyMessageRes RespondMessage(MyMessageReq message)
        {
            return new MyMessageRes { Data = "CodingByToDesign.NET" };
        }

        // requester in-proc that can get responses
        private static IRequester2<MyMessageReq, MyMessageRes> requester3 =
            new Requester2<MyMessageReq, MyMessageRes> { ResponseAction = ReceiveMessageBack3 };

        private static void ReceiveMessageBack3(MyMessageRes message)
        {
            // do what you want with received response messages here
        }

        // in-proc send/request method 3
        private static void SendMessageAsync3()
        {
            // sending example message
            requester3.RequestSync(new MyMessageReq
            {
                Uri = "http://codingbytodesign.net",
                TimeStampUtc = DateTime.UtcNow,
                HttpMethod = "GET"
            },
                // optional parameter if it is null ReceiveMessageBack3 will be used
            new Action<MyMessageRes>(
                msgRes => { /* use your response message here */ }));
        }
    }

    class MultiServiceBusDemoClassInHostOrInNet
    {
        // recevier in-host or in-net
        private static IReceiver1<MyMessageReq> receiver1 =
            new Receiver1<MyMessageReq>(
                netTcpRequesterAddress: "net.tcp://127.0.0.1:27271/Demo"
            ) { RecieveAction = ReceiveMessage };

        // receiver in-host or in-net receive method
        private static void ReceiveMessage(MyMessageReq message)
        {
            // do what you want with received messages here
        }

        // requester in-host or in-net
        private static IRequester1<MyMessageReq> requester1 =
            new Requester1<MyMessageReq>(
                netTcpRequesterAddress: "net.tcp://127.0.0.1:27271/Demo"
            );

        // receive in-host or in-net send/request method
        private static void SendMessageAsync1()
        {
            // sending example message to any Receiver<MyMessageReq>
            requester1.RequestAsync(new MyMessageReq
            {
                Uri = "http://codingbytodesign.net",
                TimeStampUtc = DateTime.UtcNow,
                HttpMethod = "GET"
            });
        }

        // receiver in-host or in-net that can response but do not have to
        private static IReceiver2<MyMessageReq, MyMessageRes> receiver2 =
            new Receiver2<MyMessageReq, MyMessageRes>(
                netTcpRequesterAddress: "net.tcp://127.0.0.1:27272/Demo"
            ) { RecieveAction = ReceiveMessage2 };

        // method that receive messages
        private static void ReceiveMessage2(TransactId id, MyMessageReq mesage)
        {
            // you can response back but you do not have to
            receiver2.ResponseAsync(id, new MyMessageRes { Data = "CodingByToDesign.NET" });
        }

        // requester in-host or in-net that can get responses
        private static IRequester2<MyMessageReq, MyMessageRes> requester2 =
            new Requester2<MyMessageReq, MyMessageRes>(
                netTcpRequesterAddress: "net.tcp://127.0.0.1:27272/Demo"
            ) { ResponseAction = ReceiveMessageBack2 };

        private static void ReceiveMessageBack2(MyMessageRes message)
        {
            // do what you want with received response messages here
        }

        // in-host or in-net send/request method 2
        private static void SendMessageAsync2()
        {
            // sending example message
            requester2.RequestAsync(new MyMessageReq
            {
                Uri = "http://codingbytodesign.net",
                TimeStampUtc = DateTime.UtcNow,
                HttpMethod = "GET"
            },
                // optional parameter if it is null ReceiveMessageBack2 will be used
            new Action<MyMessageRes>(
                msgRes => { /* use your response message here */ }));
        }

        // responder in-host or in-net that have to respond always for messages
        private static IResponder2<MyMessageReq, MyMessageRes> responder3 =
            new Responder2<MyMessageReq, MyMessageRes>(
                netTcpRequesterAddress: "net.tcp://127.0.0.1:27273/Demo"
            ) { ResponseFunc = RespondMessage };

        // method that receive messages and need to create respond message
        private static MyMessageRes RespondMessage(MyMessageReq message)
        {
            return new MyMessageRes { Data = "CodingByToDesign.NET" };
        }

        // requester in-host or in-net that can get responses
        private static IRequester2<MyMessageReq, MyMessageRes> requester3 =
            new Requester2<MyMessageReq, MyMessageRes>(
                netTcpRequesterAddress: "net.tcp://127.0.0.1:27273/Demo"
            ) { ResponseAction = ReceiveMessageBack3 };

        private static void ReceiveMessageBack3(MyMessageRes message)
        {
            // do what you want with received response messages here
        }

        // in-host or in-net send/request method 3
        private static void SendMessageAsync3()
        {
            // sending example message
            requester3.RequestSync(new MyMessageReq
            {
                Uri = "http://codingbytodesign.net",
                TimeStampUtc = DateTime.UtcNow,
                HttpMethod = "GET"
            },
                // optional parameter if it is null ReceiveMessageBack3 will be used
            new Action<MyMessageRes>(
                msgRes => { /* use your response message here */ }));
        }
    }

    class Program
    {
        static void Main()
        {
            // it is just instantiate new objects, there is no logic inside the classes yet.
            var test1 = new MultiServiceBusDemoClassInProc();
            var test2 = new MultiServiceBusDemoClassInHostOrInNet();
        }
    }
}
