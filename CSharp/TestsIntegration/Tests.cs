﻿namespace MultiServiceBusIntegrationTests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using CodingByToDesign.MultiServiceBus.Communication;

    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test00()
        {
            Console.WriteLine(@"Syncronously sending 1 message (In-Proc): (1)Requester(msg)->(2)Receiver(msg) and NO BACK...");

            long globalTestsCounter = 0;

            using (var componentRecB1 = new Receiver1<ReqMessageB>())
            using (var componentRecB2 = new Receiver1<ReqMessageB>())
            using (var componentReqB1n2 = new Requester1<ReqMessageB>())
            {
                componentRecB1.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                componentRecB2.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqB1n2.RequestAsync(new ReqMessageB { ValueInt = 1, ValueString = "Msg" });
                }

                SpinWait.SpinUntil(() => globalTestsCounter == 2, 250 * 8);

                Assert.IsTrue(globalTestsCounter == 2, "In-Proc Communication 1 Requester to 2 Receivers does not work!");
            }
        }

        [Test]
        public void Test01()
        {
            Console.WriteLine(@"Syncronously sending 1 message (In-Proc): (1)Requester(msg1)->(1)Responder(msg1,msg2)->(1)Receiver(msg1,msg2) and NO BACK...");

            long globalTestsCounter = 0;

            using (var componentReqA = new Requester1<ReqMessageA>())
            using (var componentRespA = new Responder2<ReqMessageA, ResMessageA>())
            using (var componentRecA = new Receiver1<ResMessageA>())
            {
                componentRecA.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                componentRespA.ResponseFunc = (message) =>
                {
                    ++globalTestsCounter;
                    return new ResMessageA { ValueInt = 2, ValueString = "MsgResp" };
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqA.RequestAsync(new ReqMessageA { ValueInt = 1, ValueString = "Msg" });
                }

                SpinWait.SpinUntil(() => globalTestsCounter == 2, 250 * 8);

                Assert.IsTrue(globalTestsCounter == 2, "In-Proc Communication 1 Requester to 1 Responder to 1 Receiver does not work!");
            }
        }

        [Test]
        public void Test02()
        {
            Console.WriteLine(@"Syncronously sending 1 message (In-Proc): (1)Requester(msg1,msg2)->Responder(msg1,msg2) and BACK...");

            long globalTestsCounter = 0;

            using (var componentReqHnI = new Requester2<MessageH, MessageI>())
            using (var componentResHnI = new Responder2<MessageH, MessageI>())
            {
                componentResHnI.ResponseFunc = (message) =>
                {
                    ++globalTestsCounter;
                    return new MessageI { ValueInt = 2, ValueString = "MsgResp" };
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqHnI.RequestSync(new MessageH { ValueInt = 1, ValueString = "Msg" },
                    (resData) =>
                    {
                        ++globalTestsCounter;
                    });
                }

                SpinWait.SpinUntil(() => globalTestsCounter == 2, 250 * 8);

                Assert.IsTrue(globalTestsCounter == 2, "In-Proc Communication 1 Requester2 to 1 Responder2 does not work!");
            }
        }

        [Test]
        public void Test03()
        {
            Console.WriteLine(@"Asyncronously sending 1 message (In-Proc): (1)Receiver(msg)->(1)Requester(msg) and NO BACK...");

            long globalTestsCounter = 0;

            using (var componentRecH = new Receiver1<MessageH>())
            using (var componentReqH = new Requester1<MessageH>())
            {
                componentRecH.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqH.RequestAsync(new MessageH { ValueInt = 1, ValueString = "Msg" });
                }

                SpinWait.SpinUntil(() => globalTestsCounter == 1, 250 * 8);

                Assert.IsTrue(globalTestsCounter == 1, "In-Proc Communication 1 Requester to 1 Reveiver does not work!");
            }
        }

        [Test]
        public void Test04()
        {
            Console.WriteLine(@"Asyncronously sending 1 message (In-Host): (1)Requester(msg)->(2)Receiver(msg) and NO BACK...");

            long globalTestsCounter = 0;

			// server first in a TCP/IP way
			using (var componentReqB1n2 = new Requester1<ReqMessageB>("net.tcp://127.0.0.1:27004/Test4"))
            // client after in a TCP/IP way
			using (var componentRecB1 = new Receiver1<ReqMessageB>("net.tcp://127.0.0.1:27004/Test4"))
            using (var componentRecB2 = new Receiver1<ReqMessageB>("net.tcp://127.0.0.1:27004/Test4"))
            {
                Thread.Sleep(1000);

                componentRecB1.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                componentRecB2.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqB1n2.RequestAsync(new ReqMessageB { ValueInt = 1, ValueString = "Msg" });
                }

                SpinWait.SpinUntil(() => globalTestsCounter == 2, 250 * 8);

                Assert.IsTrue(globalTestsCounter == 2, "In-Host Communication 1 Requester to 2 Receivers does not work!");
            }
        }

        [Test]
		public void Test05()
		{
			Console.WriteLine(@"Asyncronously sending 1 message (In-Host): (1)Requester(msg1,msg2)->(1)Receiver(msg1,msg2) and BACK...");

			long globalTestsCounter = 0;

			// server first in a TCP/IP way
            using (var componentReqB1n2 = new Requester2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27005/Test5"))
			// client after in a TCP/IP way
            using (var componentRecB1 = new Receiver2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27005/Test5"))
            {
                Thread.Sleep(1000);

				componentReqB1n2.ResponseAction = (message) =>
				{
					++globalTestsCounter;
				};

				componentRecB1.RecieveAction = (id, message) =>
				{
					componentRecB1.ResponseAsync(id, new ResMessageB { ValueInt = message.ValueInt, ValueString = message.ValueString });
					++globalTestsCounter;
				};

				for (int i = 0; i < 1; i++)
				{
					componentReqB1n2.RequestAsync(new ReqMessageB { ValueInt = 1, ValueString = "Msg" });
				}

				SpinWait.SpinUntil(() => globalTestsCounter == 2, 250 * 32);

                Assert.IsTrue(globalTestsCounter == 2, "In-Host Communication 1 Requester2 to 1 Receiver2 does not work!");
			}
		}

        [Test]
        public void Test06()
        {
            Console.WriteLine(@"Asyncronously sending 1 message (In-Host): (1)Requester(msg1,msg2)->(1)Receiver(msg1,msg2) and BACK...");

            long globalTestsCounter = 0;

            // server first in a TCP/IP way
            using (var componentReqB1n2 = new Requester2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27006/Test6"))
            // client after in a TCP/IP way
            using (var componentRecB1 = new Receiver2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27006/Test6"))
            {
                Thread.Sleep(1000);

                componentRecB1.RecieveAction = (id, message) =>
                {
                    componentRecB1.ResponseAsync(id, new ResMessageB { ValueInt = message.ValueInt, ValueString = message.ValueString });
                    ++globalTestsCounter;
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqB1n2.RequestAsync(new ReqMessageB { ValueInt = 1, ValueString = "Msg" },
                        (message) =>
                        {
                            ++globalTestsCounter;
                        }
                    );
                }

                SpinWait.SpinUntil(() => globalTestsCounter == 2, 250 * 8);

                Assert.IsTrue(globalTestsCounter == 2, "In-Host Communication 1 Requester2 to 1 Receiver2 does not work!");
            }
        }

        [Test]
        public void Test07()
        {
            Console.WriteLine(@"Syncronously sending 1 message (In-Host): (1)Requester(msg1,msg2)->(1)Receiver(msg1,msg2) and BACK...");

            long globalTestsCounter = 0;

            // server first in a TCP/IP way
            using (var componentReqB1n2 = new Requester2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27007/Test7"))
            // client after in a TCP/IP way
            using (var componentRecB1 = new Receiver2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27007/Test7"))
            {
                Thread.Sleep(1000);

                componentReqB1n2.ResponseAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                componentRecB1.RecieveAction = (id, message) =>
                {
                    componentRecB1.ResponseAsync(id, new ResMessageB { ValueInt = message.ValueInt, ValueString = message.ValueString });
                    ++globalTestsCounter;
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqB1n2.RequestSync(new ReqMessageB { ValueInt = 1, ValueString = "Msg" });
                }

                Assert.IsTrue(globalTestsCounter == 2, "In-Host Communication 1 Requester to 1 Receiver does not work!");
            }
        }

        [Test]
        public void Test08()
        {
            Console.WriteLine(@"Syncronously sending 1 message (In-Host): (1)Requester(msg1,msg2)->(1)Receiver(msg1,msg2) and BACK...");

            long globalTestsCounter = 0;

            // server first in a TCP/IP way
            using (var componentReqB1n2 = new Requester2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27008/Test8"))
            // client after in a TCP/IP way
            using (var componentRecB1 = new Receiver2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27008/Test8"))
            {
                Thread.Sleep(1000);

                componentRecB1.RecieveAction = (id, message) =>
                {
                    componentRecB1.ResponseAsync(id, new ResMessageB { ValueInt = message.ValueInt, ValueString = message.ValueString });
                    ++globalTestsCounter;
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqB1n2.RequestSync(new ReqMessageB { ValueInt = 1, ValueString = "Msg" },
                        (message) =>
                        {
                            ++globalTestsCounter;
                        }
                    );
                }

                Assert.IsTrue(globalTestsCounter == 2, "In-Host Communication 1 Requester2 to 1 Receiver2 does not work!");
            }
        }

        [Test]
        public void Test09()
        {
            Console.WriteLine(@"Syncronously sending 1 message (In-Host): (1)Requester(msg1,msg2)->(1)Responder(msg1,msg2) and BACK...");

            long globalTestsCounter = 0;

            // server first in a TCP/IP way
            using (var componentReqB1n2 = new Requester2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27009/Test9"))
            // client after in a TCP/IP way
            using (var componentRecB1 = new Responder2<ReqMessageB, ResMessageB>("net.tcp://127.0.0.1:27009/Test9"))
            {
                Thread.Sleep(1000);

                componentRecB1.ResponseFunc = (message) =>
                {
                    ++globalTestsCounter;
                    return new ResMessageB { ValueInt = message.ValueInt, ValueString = message.ValueString };
                };

                for (int i = 0; i < 1; i++)
                {
                    componentReqB1n2.RequestSync(new ReqMessageB { ValueInt = 1, ValueString = "Msg" },
                        (message) =>
                        {
                            ++globalTestsCounter;
                        }
                    );
                }

                Assert.IsTrue(globalTestsCounter == 2, "In-Host Communication 1 Requester2 to 1 Responder2 does not work!");
            }
        }
    }
}
