﻿namespace MultiServiceBusPerformanceTests
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using CodingByToDesign.MultiServiceBus.Communication;

    [TestFixture]
    public class Tests
    {
        [Test]
        public void TestP00()
        {
            int repeat = 3;

            Console.WriteLine(string.Format(@"Asyncronously sending {0} * 50 000 messages (In-Proc): (1)Requester(msg1,msg2)->(1)Responder(msg1,msg2) and BACK...", repeat));

            long globalTestsCounter = 0;

            var swDispose = new Stopwatch();
            using (var componentReqFnG = new Requester2<MessageF, MessageG>())
            using (var componentRespFnG = new Responder2<MessageF, MessageG>())
            {
                componentRespFnG.ResponseFunc = (message) =>
                {
                    return new MessageG { ValueInt = 2, ValueString = "Msg2" };
                };

                for (int k = 0; k < repeat; k++)
                {
                    var counter = 0;
                    Console.Write(@"Benchmark start 50 000 events: ");
                    var sw = new Stopwatch();
                    sw.Start();
                    Parallel.For(0, 50000,
                        i => componentReqFnG.RequestAsync(
                        new MessageF { ValueInt = 1, ValueString = "Msg1" },
                        message =>
                        {
                            ++globalTestsCounter;
                            ++counter;
                        })
                    );
                    SpinWait.SpinUntil(() => counter == 50000, 250 * repeat);
                    sw.Stop();

                    Console.WriteLine(string.Format(@"{0} milliseconds, {1} per second.", sw.ElapsedMilliseconds,
                                          Math.Round(((50000d / sw.ElapsedMilliseconds) * 1000d), 4)));
                }
                SpinWait.SpinUntil(() => globalTestsCounter == 50000 * repeat, 250 * repeat);

                Console.Write(string.Format("Disposing..."));
                swDispose.Start();
            }
            swDispose.Stop();
            Console.WriteLine(string.Format(" done after {0} milliseconds.", swDispose.ElapsedMilliseconds));

            Assert.IsTrue(globalTestsCounter == 50000 * repeat, "In-Proc Communication 1 Requester2 to 1 Responder2 does not work!");
        }

        [Test]
        public void TestP01()
        {
            int repeat = 3;
            Console.WriteLine(string.Format(@"Asyncronously sending {0} * 100 000 messages (In-Proc): (1)Requester(msg1,msg2)->(1)Receiver(msg1,msg2)->(1)Requester(msg3,msg4)->(1)Receiver(msg3,msg4)->(1)Requester(msg5)->(1)Receiver(msg5) and (2)BACK(msg2,msg4)...", repeat));

            long globalTestsCounter = 0;

            var swDispose = new Stopwatch();

            using (var componentReqAnB = new Requester2<MessageA, MessageB>())
            using (var componentRecAnB = new Receiver2<MessageA, MessageB>())
            using (var componentReqCnD = new Requester2<MessageC, MessageD>())
            using (var componentRecCnD = new Receiver2<MessageC, MessageD>())
            using (var componentReqE = new Requester1<MessageE>())
            using (var componentRecE = new Receiver1<MessageE>())
            {
                componentRecE.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                componentRecCnD.RecieveAction = (id, message) =>
                {
                    var resDataE = new MessageE { ValueInt = message.ValueInt + 1, ValueString = "MsgCnD" };
                    var resDataD = new MessageD { ValueInt = message.ValueInt + 1, ValueString = "MsgCnD" };

                    componentReqE.RequestAsync(resDataE);
                    componentRecCnD.ResponseAsync(id, resDataD);
                };

                componentRecAnB.RecieveAction = (id, reqData) =>
                {
                    var messageResp = new MessageC { ValueInt = 1, ValueString = "MsgAnB" };

                    componentReqCnD.RequestAsync(
                      messageResp,
                      message =>
                      {
                          if ("MsgCnD" != message.ValueString)
                          {
                              Console.WriteLine(@"Error!");
                          }

                          var resDataE = new MessageE { ValueInt = message.ValueInt + 1, ValueString = "MsgE" };
                          var resDataB = new MessageB { ValueInt = message.ValueInt + 1, ValueString = "MsgB" };

                          componentReqE.RequestAsync(resDataE);
                          componentRecAnB.ResponseAsync(id, resDataB);
                      }
                    );
                };

                var messageA = new MessageA();
                for (int k = 0; k < repeat; k++)
                {
                    int counter = 0;
                    Console.Write(@"Benchmark start 100 000 events: ");
                    var sw = new Stopwatch();
                    sw.Start();
                    Parallel.For(0, 50000,
                    i =>
                    {
                        messageA.ValueInt = 1;
                        messageA.ValueString = "OutA";

                        componentReqAnB.RequestAsync(
                            messageA,
                            message =>
                            {
                                ++counter;
                            }
                        );
                    });
                    SpinWait.SpinUntil(() => counter == 50000, 250 * repeat);
                    sw.Stop();

                    Console.WriteLine(string.Format(@"{0} milliseconds, {1} per second.", sw.ElapsedMilliseconds,
                                          Math.Round(((100000d / sw.ElapsedMilliseconds) * 1000d), 4)));
                }
                SpinWait.SpinUntil(() => globalTestsCounter == 100000 * repeat, 250 * repeat);

                Console.Write(string.Format("Disposing..."));
                swDispose.Start();
            }
            swDispose.Stop();
            Console.WriteLine(string.Format(" done after {0} milliseconds.", swDispose.ElapsedMilliseconds));

            Assert.IsTrue(globalTestsCounter == 100000 * repeat, "In-Proc Communication does not work!");
        }

        [Test]
        public void TestP02()
        {
            int repeat = 3;
            Console.WriteLine(string.Format(@"Asyncronously sending {0} * 50 000 messages (In-Host): (1)Requester(msg1)->(1)Receiver(msg1) and NO BACK...", repeat));

            long globalTestsCounter = 0;
            long counter = 0;

            var swDispose = new Stopwatch();
            using (var componentReqFnG = new Requester1<MessageF>("net.tcp://127.0.0.1:27003/PTest3"))
            using (var componentRespFnG = new Receiver1<MessageF>("net.tcp://127.0.0.1:27003/PTest3"))
            {
                Thread.Sleep(1000);

                componentRespFnG.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                    ++counter;
                };

                for (int k = 0; k < repeat; k++)
                {
                    counter = 0;
                    Console.Write(@"Benchmark start 50 000 events: ");
                    var sw = new Stopwatch();
                    sw.Start();
                    Parallel.For(0, 50000,
                        i => componentReqFnG.RequestAsync(
                        new MessageF { ValueInt = 1, ValueString = "Msg1" })
                    );
                    SpinWait.SpinUntil(() =>
                    {
                        return counter == 50000;
                    }, 250 * 8 * repeat * 50000);
                    sw.Stop();
                    Console.WriteLine(string.Format(@"{0} milliseconds, {1} per second.", sw.ElapsedMilliseconds,
                                          Math.Round(((50000d / sw.ElapsedMilliseconds) * 1000d), 4)));
                }
                SpinWait.SpinUntil(() => globalTestsCounter == 50000 * repeat, 250 * 8 * repeat * 50000);

                Console.Write(string.Format("Disposing..."));
                swDispose.Start();
            }
            swDispose.Stop();
            Console.WriteLine(string.Format(" done after {0} milliseconds.", swDispose.ElapsedMilliseconds));

            Assert.IsTrue(globalTestsCounter == 50000 * repeat, "In-Host Communication 1 Requester to 1 Receiver does not work!");
        }

        [Test]
        public void TestP03()
        {
            int repeat = 3;
            Console.WriteLine(string.Format(@"Asyncronously sending {0} * 50 000 messages (In-Host): (1)Requester(msg1,msg2)->(1)Responder(msg1,msg2) and BACK...", repeat));

            long globalTestsCounter = 0;

            var swDispose = new Stopwatch();
            using (var componentReqFnG = new Requester2<MessageF, MessageG>("net.tcp://127.0.0.1:27003/PTest4"))
            using (var componentRespFnG = new Responder2<MessageF, MessageG>("net.tcp://127.0.0.1:27003/PTest4"))
            {
                Thread.Sleep(1000);

                componentRespFnG.ResponseFunc = (message) =>
                {
                    return new MessageG { ValueInt = 2, ValueString = "Msg2" };
                };

                for (int k = 0; k < repeat; k++)
                {
                    var counter = 0;
                    Console.Write(@"Benchmark start 50 000 events: ");
                    var sw = new Stopwatch();
                    sw.Start();
                    Parallel.For(0, 50000,
                        i => componentReqFnG.RequestAsync(
                        new MessageF { ValueInt = 1, ValueString = "Msg1" },
                        message =>
                        {
                            ++globalTestsCounter;
                            ++counter;
                        })
                    );
                    SpinWait.SpinUntil(() =>
                    {
                        return counter == 50000;
                    }, 250 * 8 * repeat * 50000);
                    sw.Stop();
                    Console.WriteLine(string.Format(@"{0} milliseconds, {1} per second.", sw.ElapsedMilliseconds,
                                          Math.Round(((50000d / sw.ElapsedMilliseconds) * 1000d), 4)));
                }
                SpinWait.SpinUntil(() => globalTestsCounter == 50000 * repeat, 250 * 8 * repeat * 50000);

                Console.Write(string.Format("Disposing..."));
                swDispose.Start();
            }
            swDispose.Stop();
            Console.WriteLine(string.Format(" done after {0} milliseconds.", swDispose.ElapsedMilliseconds));

            Assert.IsTrue(globalTestsCounter == 50000 * repeat, "In-Host Communication 1 Requester2 to 1 Rsponder2 does not work!");
        }

        // todo: solve the protobuf issue and uncomment this test
        // [Test]
        public void TestP04()
        {
            int repeat = 3;
            Console.WriteLine(string.Format(@"Asyncronously sending {0} * 100 000 messages (In-Host): (1)Requester(msg1,msg2)->(1)Receiver(msg1,msg2)->(1)Requester(msg3,msg4)->(1)Receiver(msg3,msg4)->(1)Requester(msg5)->(1)Receiver(msg5) and (2)BACK(msg2,msg4)...", repeat));

            long globalTestsCounter = 0;

            var swDispose = new Stopwatch();

            using (var componentReqAnB = new Requester2<MessageA, MessageB>("net.tcp://127.0.0.1:27004/PTest5"))
            using (var componentRecAnB = new Receiver2<MessageA, MessageB>("net.tcp://127.0.0.1:27004/PTest5"))
            using (var componentReqCnD = new Requester2<MessageC, MessageD>("net.tcp://127.0.0.1:27014/PTest5"))
            using (var componentRecCnD = new Receiver2<MessageC, MessageD>("net.tcp://127.0.0.1:27014/PTest5"))
            using (var componentReqE = new Requester1<MessageE>("net.tcp://127.0.0.1:27024/PTest5"))
            using (var componentRecE = new Receiver1<MessageE>("net.tcp://127.0.0.1:27024/PTest5"))
            {
                Thread.Sleep(1000);

                componentRecE.RecieveAction = (message) =>
                {
                    ++globalTestsCounter;
                };

                componentRecCnD.RecieveAction = (id, message) =>
                {
                    var resDataE = new MessageE { ValueInt = message.ValueInt + 1, ValueString = "MsgCnD" };
                    var resDataD = new MessageD { ValueInt = message.ValueInt + 1, ValueString = "MsgCnD" };

                    componentReqE.RequestAsync(resDataE);
                    componentRecCnD.ResponseAsync(id, resDataD);
                };

                componentRecAnB.RecieveAction = (id, reqData) =>
                {
                    var messageResp = new MessageC { ValueInt = 1, ValueString = "MsgAnB" };

                    componentReqCnD.RequestAsync(
                      messageResp,
                      message =>
                      {
                          if ("MsgCnD" != message.ValueString)
                          {
                              Console.WriteLine(@"Error!");
                          }

                          var resDataE = new MessageE { ValueInt = message.ValueInt + 1, ValueString = "MsgE" };
                          var resDataB = new MessageB { ValueInt = message.ValueInt + 1, ValueString = "MsgB" };

                          componentReqE.RequestAsync(resDataE);
                          componentRecAnB.ResponseAsync(id, resDataB);
                      }
                    );
                };

                var messageA = new MessageA();
                for (int k = 0; k < repeat; k++)
                {
                    int counter = 0;
                    Console.Write(@"Benchmark start 100 000 events: ");
                    var sw = new Stopwatch();
                    sw.Start();
                    Parallel.For(0, 50000,
                    i =>
                    {
                        messageA.ValueInt = 1;
                        messageA.ValueString = "OutA";

                        componentReqAnB.RequestAsync(
                            messageA,
                            message =>
                            {
                                ++counter;
                            }
                        );
                    });
                    SpinWait.SpinUntil(() =>
                    {
                        return counter == 50000;
                    }, 250 * 8 * repeat * 50000);
                    sw.Stop();
                    Console.WriteLine(string.Format(@"{0} milliseconds, {1} per second.", sw.ElapsedMilliseconds,
                                          Math.Round(((100000d / sw.ElapsedMilliseconds) * 1000d), 4)));
                }
                SpinWait.SpinUntil(() => globalTestsCounter == 100000 * repeat, 250 * 8 * repeat * 100000);

                Console.Write(string.Format("Disposing..."));
                swDispose.Start();
            }
            swDispose.Stop();
            Console.WriteLine(string.Format(" done after {0} milliseconds.", swDispose.ElapsedMilliseconds));

            Assert.IsTrue(globalTestsCounter == 100000 * repeat, "In-Host Communication does not work!");
        }
    }
}
