﻿namespace MultiServiceBusPerformanceTests
{
    using System;
    using System.Linq;
    using CodingByToDesign.MultiServiceBus.Communication.Contracts;
    using ProtoBuf;

    [Serializable]
    [ProtoContract]
    public class BaseMessage : IMessage
    {
        [ProtoMember(1)]
        public int ValueInt { get; set; }
        [ProtoMember(2)]
        public string ValueString { get; set; }
    }

    [Serializable]
    [ProtoContract]
    public class ReqMessage : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class ResMessage : BaseMessage { }

    [Serializable]
    [ProtoContract]
    public class ReqMessageA : ReqMessage { }
    [Serializable]
    [ProtoContract]
    public class ResMessageA : ResMessage { }
    [Serializable]
    [ProtoContract]
    public class ReqMessageB : ReqMessage { }
    [Serializable]
    [ProtoContract]
    public class ResMessageB : ResMessage { }

    [Serializable]
    [ProtoContract]
    public class MessageA : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageB : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageC : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageD : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageE : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageF : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageG : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageH : BaseMessage { }
    [Serializable]
    [ProtoContract]
    public class MessageI : BaseMessage { }
}
